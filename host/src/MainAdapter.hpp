#pragma once

#include <QtCore/qobject.h>

class MainAdapter : public QObject {
    Q_OBJECT

  public:
    MainAdapter(QObject* parent = nullptr);
    ~MainAdapter();

  private:
    struct pImpl;
    pImpl* p;
};
