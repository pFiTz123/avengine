#include "Application.hpp"
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickItem>
#include <QtQuick/QQuickView>

Application::Application(int& argc, char** argv)
  : QGuiApplication(argc, argv) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    setOrganizationName("FizzLed");
    setOrganizationDomain("http://fizzled.de");
    setApplicationName("Engine");

    _view = std::make_unique<QQuickView>();
    _view->setResizeMode(QQuickView::ResizeMode::SizeRootObjectToView);
    _view->setSource(QUrl("qrc:///qml/main.qml"));
    _adapter = std::make_unique<MainAdapter>(this);
    _view->rootContext()->setContextProperty("mainAdapter", _adapter.get());
}

int32_t Application::run() {
    _view->show();
    return exec();
}
