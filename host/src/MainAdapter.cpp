#include "MainAdapter.hpp"

#include <filesystem>
#include <fstream>

struct MainAdapter::pImpl {
    pImpl() {}
};

MainAdapter::MainAdapter(QObject* parent)
  : QObject(parent)
  , p(new pImpl()) {}

MainAdapter::~MainAdapter() {}
