#pragma once

#include "MainAdapter.hpp"
#include <QtGui/qguiapplication.h>
#include <QtQuick/QQuickView>
#include <memory>

class Application : public QGuiApplication {
    Q_OBJECT
  public:
    Application(int& argc, char** argv);
    int32_t run();

  private:
    std::unique_ptr<MainAdapter> _adapter;
    std::unique_ptr<QQuickView> _view;
};
