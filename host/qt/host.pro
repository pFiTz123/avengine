QT = core qml gui widgets quick multimedia

SOURCES += ../src/main.cpp \
           ../src/Application.cpp \
           ../src/MainAdapter.cpp
		   
HEADERS += ../src/Application.hpp \
           ../src/MainAdapter.hpp
		   
RESOURCES += res.qrc
