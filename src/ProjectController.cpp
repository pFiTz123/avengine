#include "engine/project/ProjectController.hpp"
#include "project/EntryFactory.hpp"
#include "project/Project.hpp"
#include "system/Logger.hpp"
#include "system/Mutex.hpp"
#include <algorithm>
#include <limits>

namespace engine {

struct ProjectController::pImpl {
    pImpl() = default;
    Mutex mutex;
    pro::Project project;
    ReaderCreator readerCreator;
    std::vector<size_t> activeArrangers;
    std::vector<std::reference_wrapper<ProjectObserver>> observers;
};

ProjectController::ProjectController() noexcept
  : p(std::make_unique<pImpl>())
{
}

ProjectController::~ProjectController() noexcept
{
}
void ProjectController::init(const ReaderCreator& readerCreator) noexcept
{
    ScopedLock lck(p->mutex);
    p->readerCreator = readerCreator;
}

size_t ProjectController::createArranger() noexcept
{
    ScopedLock lck(p->mutex);
    return p->project.create()->id();
}

void ProjectController::setActive(size_t arrangerId) noexcept
{
    if (p->project.getArranger(arrangerId)) {
        ScopedLock lck(p->mutex);
        p->activeArrangers.clear();
        p->activeArrangers.emplace_back(arrangerId);
    }
}

void ProjectController::setActive(std::vector<size_t> arrangerIds) noexcept
{
    std::vector<size_t> arrangersToActivate;
    for (const auto& arranger : p->project.getArrangers()) {
        auto found = std::find(std::begin(arrangerIds), std::end(arrangerIds), arranger->id());
        if (found != std::end(arrangerIds)) {
            arrangersToActivate.emplace_back(*found);
        }
    }
    if (!arrangersToActivate.empty()) {
        ScopedLock lck(p->mutex);
        p->activeArrangers = arrangersToActivate;
    }
}

void ProjectController::setInActive(size_t arrangerId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->activeArrangers), std::end(p->activeArrangers), arrangerId);
    if (found != std::end(p->activeArrangers))
        p->activeArrangers.erase(found);
}

void ProjectController::setInActive(std::vector<size_t> arrangerIds) noexcept
{
    ScopedLock lck(p->mutex);
    for (size_t arrangerId : arrangerIds) {
        auto found = std::find(std::begin(p->activeArrangers), std::end(p->activeArrangers), arrangerId);
        if (found != std::end(p->activeArrangers))
            p->activeArrangers.erase(found);
    }
}

std::vector<size_t> ProjectController::getActive() const noexcept
{
    ScopedLock lck(p->mutex);
    return p->activeArrangers;
}

std::optional<pro::Entry> ProjectController::add(
    size_t arrangerId, size_t trackIdx, sys::String file, clk::Nanos start, std::optional<clk::Nanos> end) noexcept
{
    if (auto reader = p->readerCreator(file)) {
        auto entryEnd = std::min(end.value_or(clk::Nanos{std::numeric_limits<int64_t>::max()}),
                                 start + reader.value()->duration());
        if (auto entry = pro::EntryFactory::create(start, entryEnd)) {
            ScopedLock lck(p->mutex);
            p->project.addEntry(arrangerId, trackIdx, entry.value());
            for (const ProjectObserver& obs : p->observers) {
                obs.onEntryAdded(entry.value().id(), reader.value());
            }
            return entry.value();
        }
        else {
            LOGWV("Failed to add entry, assert(end > start)!, start: %lld, end: %lld", start.count(), entryEnd.count());
        }
    }
    else {
        LOGW("Failed to add entry, could not create reader!");
    }
    return std::nullopt;
}

void ProjectController::removeArranger(size_t arrangerId) noexcept
{
    ScopedLock lck(p->mutex);
    std::vector<pro::Entry> entriesToBeRemoved;
    if (const auto& tracks = p->project.getTracks(arrangerId)) {
        for (const auto& track : tracks.value()) {
            const auto& entries = track->getEntries();
            std::copy(std::begin(entries), std::end(entries), std::back_inserter(entriesToBeRemoved));
        }
    }
    auto found = std::find(std::begin(p->activeArrangers), std::end(p->activeArrangers), arrangerId);
    if (found != std::end(p->activeArrangers)) {
        p->activeArrangers.erase(found);
    }
    p->project.removeArranger(arrangerId);
    for (const auto& entry : entriesToBeRemoved) {
        for (const ProjectObserver& obs : p->observers) {
            obs.onEntryRemoved(entry.id());
        }
    }
}

void ProjectController::removeTrack(size_t arrangerId, size_t trackIdx) noexcept
{
    ScopedLock lck(p->mutex);
    std::vector<pro::Entry> entriesToBeRemoved;
    if (const auto& tracks = p->project.getTracks(arrangerId)) {
        if (!tracks.value().empty() && trackIdx < tracks.value().size()) {
            entriesToBeRemoved = tracks.value().at(trackIdx)->getEntries();
        }
    }
    p->project.removeTrack(arrangerId, trackIdx);
    for (const auto& entry : entriesToBeRemoved) {
        for (const ProjectObserver& obs : p->observers) {
            obs.onEntryRemoved(entry.id());
        }
    }
}

void ProjectController::removeEntry(size_t entryId) noexcept
{
    ScopedLock lck(p->mutex);
    p->project.removeEntry(entryId);
    for (const ProjectObserver& obs : p->observers) {
        obs.onEntryRemoved(entryId);
    }
}

void ProjectController::clear() noexcept
{
    ScopedLock lck(p->mutex);
    p->project.clearArrangers();
}

void ProjectController::clear(size_t arrangerId) noexcept
{
    ScopedLock lck(p->mutex);
    p->project.clearArranger(arrangerId);
}

void ProjectController::clear(size_t arrangerId, size_t trackIdx) noexcept
{
    ScopedLock lck(p->mutex);
    p->project.clearTrack(arrangerId, trackIdx);
}

clk::Nanos ProjectController::start() const noexcept
{
    using limit = std::numeric_limits<int64_t>;
    clk::Nanos min{limit::max()};
    ScopedLock lck(p->mutex);
    for (const auto& arranger : p->project.getArrangers()) {
        auto found = std::find(std::begin(p->activeArrangers), std::end(p->activeArrangers), arranger->id());
        if (found != std::end(p->activeArrangers)) {
            min = std::min(arranger->start(), min);
        }
    }
    return min == clk::Nanos{limit::max()} ? clk::Nanos{0} : min;
}

clk::Nanos ProjectController::start(size_t arrangerId) const noexcept
{
    clk::Nanos start{0};
    ScopedLock lck(p->mutex);
    if (auto arranger = p->project.getArranger(arrangerId)) {
        start = std::min(arranger.value()->start(), start);
    }
    return start;
}

clk::Nanos ProjectController::start(size_t arrangerId, size_t trackIdx) const noexcept
{
    clk::Nanos start{0};
    ScopedLock lck(p->mutex);
    if (auto tracks = p->project.getTracks(arrangerId)) {
        for (const auto& track : tracks.value()) {
            start = std::min(track->start(), start);
        }
    }
    return start;
}

clk::Nanos ProjectController::end() const noexcept
{
    clk::Nanos max{0};
    ScopedLock lck(p->mutex);
    for (const auto& arranger : p->project.getArrangers()) {
        auto found = std::find(std::begin(p->activeArrangers), std::end(p->activeArrangers), arranger->id());
        if (found != std::end(p->activeArrangers)) {
            max = std::max(arranger->end(), max);
        }
    }
    return max;
}

clk::Nanos ProjectController::end(size_t arrangerId) const noexcept
{
    clk::Nanos end{0};
    ScopedLock lck(p->mutex);
    if (auto arranger = p->project.getArranger(arrangerId)) {
        end = std::max(arranger.value()->end(), end);
    }
    return end;
}

clk::Nanos ProjectController::end(size_t arrangerId, size_t trackIdx) const noexcept
{
    clk::Nanos end{0};
    ScopedLock lck(p->mutex);
    if (auto tracks = p->project.getTracks(arrangerId)) {
        for (const auto& track : tracks.value()) {
            end = std::max(track->end(), end);
        }
    }
    return end;
}

clk::Nanos ProjectController::duration() const noexcept
{
    return end() - start();
}

std::optional<clk::Nanos> ProjectController::duration(size_t arrangerId) const noexcept
{
    return end(arrangerId) - start(arrangerId);
}

std::optional<clk::Nanos> ProjectController::duration(size_t arrangerId, size_t trackIdx) const noexcept
{
    return end(arrangerId, trackIdx) - start(arrangerId, trackIdx);
}

void ProjectController::addObserver(ProjectObserver& observer) const noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find_if(
        std::begin(p->observers), std::end(p->observers), [&observer](ProjectObserver& o) { return &observer == &o; });
    if (found == std::end(p->observers)) {
        p->observers.emplace_back(observer);
    }
}

void ProjectController::removeObserver(ProjectObserver& observer) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find_if(
        std::begin(p->observers), std::end(p->observers), [&observer](ProjectObserver& o) { return &observer == &o; });
    if (found != std::end(p->observers)) {
        p->observers.erase(found);
    }
}

pro::TimeInfos ProjectController::at(clk::Nanos position) const noexcept
{
    ScopedLock lck(p->mutex);
    pro::TimeInfos timeInfos;
    for (const auto arrangerId : p->activeArrangers) {
        const auto& arrangerTimeInfos = p->project.at(position);
        std::copy(std::begin(arrangerTimeInfos), std::end(arrangerTimeInfos), std::back_inserter(timeInfos));
    }
    return timeInfos;
}

pro::TimeInfos ProjectController::within(clk::Nanos start, clk::Nanos end) const noexcept
{
    ScopedLock lck(p->mutex);
    pro::TimeInfos timeInfos;
    for (const auto arrangerId : p->activeArrangers) {
        const auto& arrangerTimeInfos = p->project.within(start, end);
        std::copy(std::begin(arrangerTimeInfos), std::end(arrangerTimeInfos), std::back_inserter(timeInfos));
    }
    return timeInfos;
}

std::optional<pro::TimeInfos> ProjectController::at(size_t arrangerId, clk::Nanos position) const noexcept
{
    return p->project.at(arrangerId, position);
}

std::optional<pro::TimeInfos> ProjectController::within(size_t arrangerId, clk::Nanos start, clk::Nanos end) const
    noexcept
{
    return p->project.within(arrangerId, start, end);
}
} // namespace engine
