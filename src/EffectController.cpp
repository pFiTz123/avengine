#include "engine/dsp/EffectController.hpp"
#include "dsp/EffectMapper.hpp"
#include "system/Mutex.hpp"
#include "system/String.hpp"

namespace {
size_t hash(const sys::String& s)
{
    static auto hasher = std::hash<sys::String>();
    return hasher(s);
}
} // namespace

namespace engine {

struct EffectController::pImpl {
    pImpl() = default;
    Mutex mutex;
    audio::EffectMapper arranger;
    audio::EffectMapper track;
    audio::EffectMapper entry;
    std::vector<std::pair<size_t, size_t>> arrangerTrackIdx;
    std::vector<size_t> trackIds;
};

EffectController::EffectController() noexcept
  : p(std::make_unique<pImpl>())
{
}

EffectController::~EffectController() noexcept
{
}

// Arranger
void EffectController::addArrangerEffect(size_t arrangerId, audio::EffectPtr effect) noexcept
{
    p->arranger.add(arrangerId, effect);
}

std::vector<audio::EffectPtr> EffectController::getArrangerEffects(size_t arrangerId) const noexcept
{
    return p->arranger.get(arrangerId);
}

void EffectController::removeArrangerEffect(size_t arrangerId, size_t effectId) noexcept
{
    p->arranger.remove(arrangerId, effectId);
}

void EffectController::removeArrangerEffects(size_t arrangerId) noexcept
{
    p->arranger.removeAll(arrangerId);
}

// Track
void EffectController::addTrackEffect(size_t arrangerId, size_t trackIdx, audio::EffectPtr effect) noexcept
{
    ScopedLock lck(p->mutex);
    size_t trackId(0);
    auto found = std::find(
        std::begin(p->arrangerTrackIdx), std::end(p->arrangerTrackIdx), std::make_pair(arrangerId, trackIdx));
    if (found == std::end(p->arrangerTrackIdx)) {
        p->arrangerTrackIdx.emplace_back(std::make_pair(arrangerId, trackIdx));
        trackId = p->trackIds.emplace_back(hash(sys::format("%d_%d", arrangerId, trackIdx)));
    }
    else {
        auto idx = std::distance(std::begin(p->arrangerTrackIdx), found);
        trackId = p->trackIds.at(idx);
    }
    p->track.add(trackId, effect);
}

std::vector<audio::EffectPtr> EffectController::getTrackEffects(size_t arrangerId, size_t trackIdx) const noexcept
{
    ScopedLock lck(p->mutex);
    std::vector<audio::EffectPtr> trackEffects;
    auto found = std::find(
        std::begin(p->arrangerTrackIdx), std::end(p->arrangerTrackIdx), std::make_pair(arrangerId, trackIdx));
    if (found != std::end(p->arrangerTrackIdx)) {
        auto idx = std::distance(std::begin(p->arrangerTrackIdx), found);
        trackEffects = p->track.get(p->trackIds.at(idx));
    }
    return trackEffects;
}

void EffectController::removeTrackEffect(size_t arrangerId, size_t trackIdx, size_t effectId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(
        std::begin(p->arrangerTrackIdx), std::end(p->arrangerTrackIdx), std::make_pair(arrangerId, trackIdx));
    if (found != std::end(p->arrangerTrackIdx)) {
        auto idx = std::distance(std::begin(p->arrangerTrackIdx), found);
        auto trackId = p->trackIds.at(idx);
        p->track.remove(trackId, effectId);
        if (p->track.get(trackId).empty()) {
            p->trackIds.erase(std::begin(p->trackIds) + idx);
            p->arrangerTrackIdx.erase(found);
        }
    }
}

void EffectController::removeTrackEffects(size_t arrangerId, size_t trackIdx) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(
        std::begin(p->arrangerTrackIdx), std::end(p->arrangerTrackIdx), std::make_pair(arrangerId, trackIdx));
    if (found != std::end(p->arrangerTrackIdx)) {
        auto idx = std::distance(std::begin(p->arrangerTrackIdx), found);
        auto trackId = p->trackIds.at(idx);
        p->track.removeAll(trackId);
        p->trackIds.erase(std::begin(p->trackIds) + idx);
        p->arrangerTrackIdx.erase(found);
    }
}

// Entry
void EffectController::addEntryEffect(size_t entryId, audio::EffectPtr effect) noexcept
{
    p->entry.add(entryId, effect);
}

std::vector<audio::EffectPtr> EffectController::getEntryEffects(size_t entryId) const noexcept
{
    return p->entry.get(entryId);
}

void EffectController::removeEntryEffect(size_t entryId, size_t effectId) noexcept
{
    p->entry.remove(entryId, effectId);
}

void EffectController::removeEntryEffects(size_t entryId) noexcept
{
    p->entry.removeAll(entryId);
}

void EffectController::clear() noexcept
{
    p->arranger.clear();
    p->track.clear();
    p->trackIds.clear();
    p->arrangerTrackIdx.clear();
    p->entry.clear();
}
} // namespace engine
