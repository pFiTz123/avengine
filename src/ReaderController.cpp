#include "engine/reader/ReaderController.hpp"
#include "reader/ReaderCache.hpp"
#include "reader/ReaderOffset.hpp"
#include "reader/ReaderPool.hpp"
#include "system/Mutex.hpp"

namespace engine {

struct ReaderController::pImpl {
    pImpl(audio::ReaderFactoryPtr readerFactory, const audio::SampleFormat& sampleFormat)
      : readerFactory(readerFactory)
      , sampleFormat(sampleFormat)
      , readerCache(std::make_unique<ReaderCache>(sampleFormat))
      , readerPool(std::make_unique<ReaderPool>())
      , readerOffset(std::make_unique<ReaderOffset>())
    {
    }
    Mutex mutex;
    audio::ReaderFactoryPtr readerFactory;
    audio::SampleFormat sampleFormat;
    std::unique_ptr<ReaderPool> readerPool;
    std::unique_ptr<ReaderOffset> readerOffset;
    std::unique_ptr<ReaderCache> readerCache;

    using Ids = std::vector<size_t>;
    Ids entryIds;
    Ids readerIds;
};

ReaderController::ReaderController(audio::ReaderFactoryPtr readerFactory,
                                   const audio::SampleFormat& sampleFormat) noexcept
  : p(std::make_unique<pImpl>(std::move(readerFactory), sampleFormat))
{
}

ReaderController::~ReaderController() noexcept
{
}

// Format
void ReaderController::setSampleFormat(const audio::SampleFormat& format) noexcept
{
    ScopedLock lck(p->mutex);
    p->sampleFormat = format;
    p->readerCache->clearAll();
    p->readerCache = std::make_unique<ReaderCache>(format);
    for (const auto& reader : p->readerPool->get()) {
        reader->setFormat(format);
    }
}

const audio::SampleFormat& ReaderController::sampleFormat() const noexcept
{
    ScopedLock lck(p->mutex);
    return p->sampleFormat;
}

// Cache
void ReaderController::addCache(size_t readerId, audio::Buffer buffer, clk::Nanos pos, clk::Nanos dur) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->readerIds), std::end(p->readerIds), readerId);
    if (found != std::end(p->readerIds)) {
        p->readerCache->add(readerId, std::move(buffer), pos, dur);
    }
}

std::optional<audio::Buffer> ReaderController::getCache(size_t readerId, clk::Nanos pos, clk::Nanos dur) const noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->readerIds), std::end(p->readerIds), readerId);
    if (found != std::end(p->readerIds)) {
        return p->readerCache->get(readerId, pos, dur);
    }
    return std::nullopt;
}

void ReaderController::removeCache(size_t readerId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->readerIds), std::end(p->readerIds), readerId);
    if (found != std::end(p->readerIds)) {
        p->readerCache->clear(readerId);
    }
}

void ReaderController::clearCaches() noexcept
{
    p->readerCache->clearAll();
}

// Offset
void ReaderController::setOffset(size_t readerId, clk::Nanos offset) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->readerIds), std::end(p->readerIds), readerId);
    if (found != std::end(p->readerIds))
        p->readerOffset->set(readerId, offset);
}

std::optional<clk::Nanos> ReaderController::getOffset(size_t readerId) const noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->readerIds), std::end(p->readerIds), readerId);
    if (found != std::end(p->readerIds))
        return p->readerOffset->get(readerId);
    return std::nullopt;
}

void ReaderController::removeOffset(size_t readerId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->readerIds), std::end(p->readerIds), readerId);
    if (found != std::end(p->readerIds))
        p->readerOffset->remove(readerId);
}

void ReaderController::clearOffsets() noexcept
{
    p->readerOffset->clear();
}

// Reader
void ReaderController::addReader(size_t entryId, audio::ReaderPtr reader) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->entryIds), std::end(p->entryIds), entryId);
    if (found == std::end(p->entryIds)) {
        p->entryIds.emplace_back(entryId);
        p->readerIds.emplace_back(reader->id());
        p->readerPool->add(reader);
    }
}

void ReaderController::removeReader(size_t entryId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->entryIds), std::end(p->entryIds), entryId);
    if (found != std::end(p->entryIds)) {
        auto idx = std::distance(std::begin(p->entryIds), found);
        p->readerPool->remove(p->readerIds.at(idx));
        p->entryIds.erase(found);
        p->readerIds.erase(std::begin(p->readerIds) + idx);
    }
}

std::optional<audio::ReaderPtr> ReaderController::getReader(const sys::String& file) const noexcept
{
    std::optional<audio::ReaderPtr> ret = p->readerPool->get(file);
    if (!ret.has_value()) {
        ret = p->readerFactory->create(file, p->sampleFormat);
    }
    return ret;
}

std::optional<audio::ReaderPtr> ReaderController::getReader(size_t entryId) const noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->entryIds), std::end(p->entryIds), entryId);
    if (found != std::end(p->entryIds)) {

        auto idx = std::distance(std::begin(p->entryIds), found);
        return p->readerPool->get(p->readerIds.at(idx));
    }
    return std::nullopt;
}

void ReaderController::clearReaders() noexcept
{
    ScopedLock lck(p->mutex);
    p->readerCache->clearAll();
    p->readerPool->clear();
    p->readerOffset->clear();
    p->entryIds.clear();
    p->readerIds.clear();
}

} // namespace engine
