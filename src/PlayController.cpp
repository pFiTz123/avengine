#include "engine/play/PlayController.hpp"
#include "system/Logger.hpp"
#include <algorithm>
#include <condition_variable>
#include <mutex>
#include <optional>
#include <thread>
#include <utility>

namespace engine {

struct PlayController::pImpl {
    pImpl() = default;
    ~pImpl()
    {
        audioThreadTerminate = true;
        readCondition.notify_one();

        if (audioThread->joinable()) {
            audioThread->join();
        }
    }

    void changeState(State state) noexcept;
    void reset() noexcept;
    void read() noexcept;
    bool validate() noexcept;
    audio::Buffer process(clk::Nanos duration) noexcept;

    PlayInfoProvider playInfoProvider;
    clk::Nanos position{0};
    bool isLooping = false;
    std::optional<AudioSink> audioSink;

    State state = State::Stopped;
    std::vector<StateObserver> stateObservers;

    std::mutex readMutex;
    std::condition_variable readCondition;
    bool audioThreadRunning = false;
    bool audioThreadTerminate = false;
    std::unique_ptr<std::thread> audioThread;
};

PlayController::PlayController()
  : p(std::make_unique<pImpl>())
{
}

PlayController::~PlayController()
{
}

void PlayController::init(const PlayInfoProvider& playInfoProvider) noexcept
{
    std::scoped_lock<std::mutex> lck(p->readMutex);
    p->playInfoProvider = playInfoProvider;
    p->position = playInfoProvider.startProvider();
    p->audioThread = std::make_unique<std::thread>(std::bind(&PlayController::pImpl::read, p.get()));
}

void PlayController::play() noexcept
{
    std::scoped_lock<std::mutex> lck(p->readMutex);
    if (!p->validate()) {
        LOGW("Ignoring play call, invalid duration!");
        return;
    }

    if (p->state == State::Playing) {
        LOGW("Already playing!");
    }
    else {
        p->audioThreadRunning = true;
        p->readCondition.notify_one();
        p->changeState(State::Playing);
    }
}

void PlayController::seek(clk::Nanos position) noexcept
{
    std::scoped_lock<std::mutex> lck(p->readMutex);
    if (!p->validate())
        return;
    p->position = position;
}

void PlayController::pause() noexcept
{
    std::scoped_lock<std::mutex> lck(p->readMutex);
    if (!p->validate()) {
        LOGW("Ignoring pause call, invalid duration!");
        return;
    }

    if (p->state == State::Paused) {
        LOGW("Already paused!");
    }
    else if (p->state == State::Stopped) {
        LOGW("Pause ignored, stopped!");
    }
    else {
        p->audioThreadRunning = false;
        p->readCondition.notify_one();
        p->changeState(State::Paused);
    }
}

void PlayController::stop() noexcept
{
    std::scoped_lock<std::mutex> lck(p->readMutex);
    if (!p->validate()) {
        LOGW("Ignoring stop call, invalid duration!");
        return;
    }

    if (p->state == State::Stopped) {
        LOGW("Already stopped!");
    }
    else {
        p->audioThreadRunning = false;
        p->readCondition.notify_one();
        p->changeState(State::Stopped);
        p->reset();
    }
}

State PlayController::state() const noexcept
{
    return p->state;
}

void PlayController::addObserver(StateObserver observer) noexcept
{
    p->stateObservers.emplace_back(observer);
}

void PlayController::setAudioSink(AudioSink audioSink) noexcept
{
    p->audioSink = audioSink;
}

clk::Nanos PlayController::start() const noexcept
{
    return p->playInfoProvider.startProvider();
}

clk::Nanos PlayController::end() const noexcept
{
    return p->playInfoProvider.endProvider();
}

clk::Nanos PlayController::position() const noexcept
{
    return p->position - start();
}

clk::Nanos PlayController::duration() const noexcept
{
    return end() - start();
}

void PlayController::setLooping(bool loop) noexcept
{
    p->isLooping = loop;
}

bool PlayController::isLooping() const noexcept
{
    return p->isLooping;
}

audio::Buffer PlayController::request(clk::Nanos duration) noexcept
{
    return p->process(duration);
}

void PlayController::preload(clk::Nanos dur) noexcept
{
}

void PlayController::pImpl::changeState(State state) noexcept
{
    this->state = state;
    for (const auto& observer : stateObservers) {
        observer(state);
    }
}

void PlayController::pImpl::reset() noexcept
{
    position = playInfoProvider.startProvider();
}

audio::Buffer PlayController::pImpl::process(clk::Nanos dur) noexcept
{
    std::scoped_lock<std::mutex> lck(readMutex);

    auto start = playInfoProvider.startProvider();
    auto end = playInfoProvider.endProvider();

    if (end.count() - start.count() <= 0) {
        changeState(State::Empty);
        return audio::Buffer();
    }

    if (position == end && !isLooping)
        return audio::Buffer();

    auto requestDuration = std::clamp(position + dur, start, end) - position;
    if (requestDuration.count() <= 0) {
        return audio::Buffer();
    }

    auto buffer = playInfoProvider.bufferProvider(position, requestDuration);
    position += requestDuration;
    if (position == end && isLooping) {
        position = start;
    }

    if (requestDuration < dur && isLooping) {
        requestDuration = dur - requestDuration;
        auto extended = playInfoProvider.bufferProvider(position, requestDuration);
        buffer.insert(std::end(buffer), std::begin(extended), std::end(extended));
        position += requestDuration;
    }

    return buffer;
}

void PlayController::pImpl::read() noexcept
{
    do {
        {
            std::unique_lock<std::mutex> lck(readMutex);
            readCondition.wait(lck, [this]() {
                return audioThreadTerminate
                       || (audioThreadRunning && validate() && position < playInfoProvider.endProvider());
            });
            if (audioThreadTerminate) {
                break;
            }
        }

        auto s = clk::now();
        clk::Nanos requestDuration = playInfoProvider.requestDurationProvider();
        if (audioSink.has_value()) {
            auto buffer = process(requestDuration);
            audioSink.value()(buffer, position, requestDuration);
        }
        std::this_thread::sleep_for(requestDuration - (clk::now() - s));

    } while (!audioThreadTerminate);
}

bool PlayController::pImpl::validate() noexcept
{
    auto isValid = (playInfoProvider.endProvider() - playInfoProvider.startProvider()).count() > 0;
    if (!isValid) {
        changeState(State::Empty);
    }
    return isValid;
}

bool PlayController::isPlaying() const noexcept
{
    return p->state == State::Playing;
}

} // namespace engine
