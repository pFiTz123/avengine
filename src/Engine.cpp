#include <utility>
#include "engine/Engine.hpp"
#include "engine/Processor.hpp"
#include "system/Logger.hpp"

namespace engine {

struct Engine::pImpl {
    explicit pImpl(ReaderControllerPtr readerCtrl,
                   ProjectControllerPtr projectCtrl,
                   PlayControllerPtr playCtrl,
                   EffectControllerPtr fxCtrl)
      : readerController(std::move(readerCtrl))
      , projectController(std::move(projectCtrl))
      , playController(std::move(playCtrl))
      , effectController(std::move(fxCtrl))
      , projectObserver{[ctrl = readerController.get()](size_t entryId, audio::ReaderPtr reader) {
                            ctrl->addReader(entryId, reader);
                        },
                        [ctrl = readerController.get()](size_t id) { ctrl->removeReader(id); }}
    {
        projectController->init([ctrl = readerController.get()](const auto& file) { return ctrl->getReader(file); });
        playController->init(PlayInfoProvider{
            [rCtrl = readerController.get(), pCtrl = projectController.get(), eCtrl = effectController.get()](
                clk::Nanos pos, clk::Nanos dur) { return process(rCtrl, pCtrl, eCtrl, pos, dur); },
            [ctrl = projectController.get()]() { return ctrl->start(); },
            [ctrl = projectController.get()]() { return ctrl->end(); },
            [duration = requestDuration]() { return duration; }});

        projectController->addObserver(projectObserver);
    }
    ~pImpl()
    {
        projectController->removeObserver(projectObserver);
    }

    clk::Nanos requestDuration = DEFAULT_REQUEST_DURATION;
    ReaderControllerPtr readerController = nullptr;
    ProjectControllerPtr projectController = nullptr;
    PlayControllerPtr playController = nullptr;
    EffectControllerPtr effectController = nullptr;
    ProjectObserver projectObserver;
};

Engine::Engine(ReaderControllerPtr readerCtrl,
               ProjectControllerPtr projectCtrl,
               PlayControllerPtr playCtrl,
               EffectControllerPtr fxCtrl)
  : p(std::make_unique<pImpl>(readerCtrl, projectCtrl, playCtrl, fxCtrl))
{
}

Engine::~Engine()
{
}

PlayControllerPtr Engine::playController() const noexcept
{
    return p->playController;
}
EffectControllerPtr Engine::effectController() const noexcept
{
    return p->effectController;
}

ReaderControllerPtr Engine::readerController() const noexcept
{
    return p->readerController;
}

ProjectControllerPtr Engine::projectController() const noexcept
{
    return p->projectController;
}

} // namespace engine
