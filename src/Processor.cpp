#include "engine/Processor.hpp"
#include "dsp/Mix.hpp"
#include "system/Logger.hpp"
#include "system/Utility.hpp"
#include <algorithm>
#include <tuple>
#include <vector>

namespace {
struct MixInfo {
    size_t arrangerId;
    size_t trackIdx;
    size_t entryId;
    audio::ReaderPtr reader;
    clk::Nanos position;
    clk::Nanos duration;
};

using MixInfos = std::vector<MixInfo>;
using ReaderInfo = std::tuple<audio::ReaderPtr, clk::Nanos, clk::Nanos>;
std::optional<ReaderInfo>
    readerInfoFor(engine::IReaderController* ctrl, const pro::Entry& e, clk::Nanos pos, clk::Nanos dur)
{
    std::optional<ReaderInfo> ret;
    if (auto reader = ctrl->getReader(e.id())) {
        auto readerPos = std::max(clk::Nanos{0}, pos - e.start());
        auto readerDur = std::min(e.start() <= pos ? e.end() - pos : pos + dur - e.start(), dur);
        if (auto offset = ctrl->getOffset(reader.value()->id()))
            readerPos += offset.value();
        ret = std::make_tuple(reader.value(), readerPos, readerDur);
    }
    return ret;
}

MixInfos mixInfosFor(engine::IProjectController* projectController,
                     engine::IReaderController* readerController,
                     size_t arrangerId,
                     clk::Nanos pos,
                     clk::Nanos dur)
{
    MixInfos mixInfos;
    if (const auto& arrangerTimeInfo = projectController->within(arrangerId, pos, pos + dur)) {
        for (const auto& [arranger, track, entries] : arrangerTimeInfo.value()) {
            for (const auto& entry : entries) {
                if (auto readerInfo = readerInfoFor(readerController, entry, pos, dur)) {
                    const auto& [reader, readerPos, readerDur] = readerInfo.value();
                    mixInfos.emplace_back(
                        MixInfo{arranger->id(), track->index(), entry.id(), reader, readerPos, readerDur});
                }
            }
        }
    }
    return mixInfos;
}

std::vector<audio::Buffer> buffersFrom(engine::IEffectController* ctrl, MixInfos&& mixInfos)
{
    std::vector<audio::Buffer> buffers;
    for (const auto& mixInfo : mixInfos) {
        // if (auto bufferOpt = readerCache.get(reader->id(), pos, dur)) {
        //    buffers.emplace_back(bufferOpt.value());
        //}
        // else {
        mixInfo.reader->seek(mixInfo.position);
        auto& buffer = buffers.emplace_back(mixInfo.reader->get(mixInfo.duration));
        // readerCache.add(reader->id(), buffer, pos, dur);
        //}

        // apply entry effects
        for (const auto& entryEffect : ctrl->getEntryEffects(mixInfo.entryId)) {
            entryEffect->apply(buffer);
        }
        // apply track effects
        for (const auto& trackEffect : ctrl->getTrackEffects(mixInfo.arrangerId, mixInfo.trackIdx)) {
            trackEffect->apply(buffer);
        }
    }
    return buffers;
}
} // namespace

namespace engine {

audio::Buffer process(engine::IReaderController* rCtrl,
                      engine::IProjectController* pCtrl,
                      engine::IEffectController* eCtrl,
                      clk::Nanos pos,
                      clk::Nanos dur)
{
    std::vector<audio::Buffer> arrangerBuffers;
    for (const auto arrangerId : pCtrl->getActive()) {
        auto mixInfos = mixInfosFor(pCtrl, rCtrl, arrangerId, pos, dur);
        if (!mixInfos.empty()) {
            auto buffers = buffersFrom(eCtrl, std::move(mixInfos));
            if (!buffers.empty()) {
                auto result = audio::mix(std::move(buffers));
                if (!result.empty()) {
                    for (const auto& arrangerEffect : eCtrl->getArrangerEffects(arrangerId)) {
                        arrangerEffect->apply(result);
                    }
                    arrangerBuffers.emplace_back(result);
                }
            }
        }
    }
    if (arrangerBuffers.empty()) {
        const auto& fmt = rCtrl->sampleFormat();
        return audio::Buffer(audio::toSamples(fmt.sampleRate, fmt.channels, dur));
    }
    else if (arrangerBuffers.size() == 1) {
        return arrangerBuffers.at(0);
    }
    else {
        return audio::mix(std::move(arrangerBuffers));
    }
}

} // namespace engine
