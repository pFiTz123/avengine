#pragma once

#include "IPlayController.hpp"

namespace engine {
class PlayController : public IPlayController {
  public:
    PlayController();
    PlayController(const PlayController&) = delete;
    PlayController operator=(const PlayController&) = delete;
    PlayController operator=(PlayController&&) = delete;
    PlayController(PlayController&&) = delete;
    ~PlayController() override;
    void init(const PlayInfoProvider& playInfoProvider) noexcept override;
    void play() noexcept override;
    void seek(clk::Nanos position) noexcept override;
    void pause() noexcept override;
    void stop() noexcept override;
    State state() const noexcept override;
    void addObserver(StateObserver observer) noexcept override;
    void setAudioSink(AudioSink audioSink) noexcept override;
    clk::Nanos start() const noexcept override;
    clk::Nanos end() const noexcept override;
    clk::Nanos position() const noexcept override;
    clk::Nanos duration() const noexcept override;
    void setLooping(bool loop = true) noexcept override;
    bool isLooping() const noexcept override;
    bool isPlaying() const noexcept override;
    audio::Buffer request(clk::Nanos duration) noexcept override;
    void preload(clk::Nanos duration) noexcept override;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace engine
