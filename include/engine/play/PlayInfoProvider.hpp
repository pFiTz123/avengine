#pragma once

#include "system/Buffer.hpp"
#include "system/Time.hpp"
#include <functional>

namespace engine {
using BufferProvider = std::function<audio::Buffer(clk::Nanos, clk::Nanos)>;
using TimeProvider = std::function<clk::Nanos()>;
struct PlayInfoProvider {
    BufferProvider bufferProvider;
    TimeProvider startProvider;
    TimeProvider endProvider;
    TimeProvider requestDurationProvider;
};
} // namespace engine
