#pragma once

#include "PlayInfoProvider.hpp"
#include "system/Buffer.hpp"
#include "system/Time.hpp"
#include <functional>
#include <memory>

namespace engine {

using AudioSink = std::function<void(const audio::Buffer&, clk::Nanos, clk::Nanos)>;
enum class State { Playing, Paused, Stopped, Empty };
using StateObserver = std::function<void(State)>;

struct IPlayController {

    virtual ~IPlayController() = default;
    virtual void init(const PlayInfoProvider& playInfoProvider) noexcept = 0;
    virtual void play() noexcept = 0;
    virtual void seek(clk::Nanos position) noexcept = 0;
    virtual void pause() noexcept = 0;
    virtual void stop() noexcept = 0;
    virtual State state() const noexcept = 0;
    virtual void addObserver(StateObserver observer) noexcept = 0;
    virtual clk::Nanos start() const noexcept = 0;
    virtual clk::Nanos end() const noexcept = 0;
    virtual clk::Nanos position() const noexcept = 0;
    virtual clk::Nanos duration() const noexcept = 0;
    virtual void setLooping(bool loop) noexcept = 0;
    virtual bool isLooping() const noexcept = 0;
    virtual bool isPlaying() const noexcept = 0;
    virtual audio::Buffer request(clk::Nanos duration) noexcept = 0;
    virtual void preload(clk::Nanos duration) noexcept = 0;
    virtual void setAudioSink(AudioSink audioSink) noexcept = 0;
};
using PlayControllerPtr = std::shared_ptr<IPlayController>;
} // namespace engine
