#pragma once

#include "dsp/IEffectController.hpp"
#include "project/IProjectController.hpp"
#include "reader/IReaderController.hpp"

namespace engine {
audio::Buffer process(engine::IReaderController* rCtrl,
                      engine::IProjectController* pCtrl,
                      engine::IEffectController* eCtrl,
                      clk::Nanos pos,
                      clk::Nanos dur);
} // namespace engine
