#pragma once

#include "reader/IReaderFactory.hpp"
#include "system/Buffer.hpp"
#include "system/SampleFormat.hpp"
#include <memory>

namespace engine {
struct IReaderController {

    virtual ~IReaderController() noexcept = default;

    // Reader
    virtual void addReader(size_t entryId, audio::ReaderPtr) noexcept = 0;
    virtual void removeReader(size_t entryId) noexcept = 0;
    virtual std::optional<audio::ReaderPtr> getReader(const sys::String& file) const noexcept = 0;
    virtual std::optional<audio::ReaderPtr> getReader(size_t entryId) const noexcept = 0;
    virtual void clearReaders() noexcept = 0;

    // Format
    virtual void setSampleFormat(const audio::SampleFormat& format) noexcept = 0;
    virtual const audio::SampleFormat& sampleFormat() const noexcept = 0;

    // Cache
    virtual void addCache(size_t readerId, audio::Buffer buffer, clk::Nanos pos, clk::Nanos dur) noexcept = 0;
    virtual std::optional<audio::Buffer> getCache(size_t readerId, clk::Nanos pos, clk::Nanos dur) const noexcept = 0;
    virtual void removeCache(size_t readerId) noexcept = 0;
    virtual void clearCaches() noexcept = 0;

    // Offset
    virtual void setOffset(size_t readerId, clk::Nanos offset) noexcept = 0;
    virtual std::optional<clk::Nanos> getOffset(size_t readerId) const noexcept = 0;
    virtual void removeOffset(size_t readerId) noexcept = 0;
    virtual void clearOffsets() noexcept = 0;
};
using ReaderControllerPtr = std::shared_ptr<IReaderController>;
} // namespace engine
