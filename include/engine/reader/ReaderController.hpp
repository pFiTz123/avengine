#pragma once

#include "IReaderController.hpp"

namespace engine {
class ReaderController : public IReaderController {
  public:
    ReaderController(audio::ReaderFactoryPtr readerFactory, const audio::SampleFormat& sampleFormat) noexcept;
    ~ReaderController() noexcept;

    // Reader
    void addReader(size_t entryId, audio::ReaderPtr) noexcept override;
    void removeReader(size_t entryId) noexcept override;
    std::optional<audio::ReaderPtr> getReader(const sys::String& file) const noexcept override;
    std::optional<audio::ReaderPtr> getReader(size_t entryId) const noexcept override;
    void clearReaders() noexcept override;

    // Format
    void setSampleFormat(const audio::SampleFormat& format) noexcept override;
    const audio::SampleFormat& sampleFormat() const noexcept override;

    // Cache
    void addCache(size_t readerId, audio::Buffer buffer, clk::Nanos pos, clk::Nanos dur) noexcept override;
    std::optional<audio::Buffer> getCache(size_t readerId, clk::Nanos pos, clk::Nanos dur) const noexcept override;
    void removeCache(size_t readerId) noexcept override;
    void clearCaches() noexcept override;

    // Offset
    void setOffset(size_t readerId, clk::Nanos offset) noexcept override;
    std::optional<clk::Nanos> getOffset(size_t readerId) const noexcept override;
    void removeOffset(size_t readerId) noexcept override;
    void clearOffsets() noexcept override;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace engine
