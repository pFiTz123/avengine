#pragma once

#include "IProjectController.hpp"

namespace engine {

class ProjectController : public IProjectController {
  public:
    ProjectController() noexcept;
    ~ProjectController() noexcept;
    void init(const ReaderCreator& readerCreator) noexcept override;
    size_t createArranger() noexcept override;
    void setActive(size_t arrangerId) noexcept override;
    void setActive(std::vector<size_t> arrangerIds) noexcept override;
    void setInActive(size_t arrangerId) noexcept override;
    void setInActive(std::vector<size_t> arrangerIds) noexcept override;
    std::vector<size_t> getActive() const noexcept override;

    std::optional<pro::Entry> add(size_t arrangerId,
                                  size_t trackIdx,
                                  sys::String file,
                                  clk::Nanos start,
                                  std::optional<clk::Nanos> end = std::nullopt) noexcept override;

    void removeArranger(size_t arrangerId) noexcept override;
    void removeTrack(size_t arrangerId, size_t trackIdx) noexcept override;
    void removeEntry(size_t entryId) noexcept override;

    void clear() noexcept override;
    void clear(size_t arrangerId) noexcept override;
    void clear(size_t arrangerId, size_t trackIdx) noexcept override;

    clk::Nanos start() const noexcept override;
    clk::Nanos start(size_t arrangerId) const noexcept override;
    clk::Nanos start(size_t arrangerId, size_t trackIdx) const noexcept override;

    clk::Nanos end() const noexcept override;
    clk::Nanos end(size_t arrangerId) const noexcept override;
    clk::Nanos end(size_t arrangerId, size_t trackIdx) const noexcept override;

    clk::Nanos duration() const noexcept override;
    std::optional<clk::Nanos> duration(size_t arrangerId) const noexcept override;
    std::optional<clk::Nanos> duration(size_t arrangerId, size_t trackIdx) const noexcept override;

    pro::TimeInfos at(clk::Nanos position) const noexcept override;
    pro::TimeInfos within(clk::Nanos start, clk::Nanos end) const noexcept override;
    std::optional<pro::TimeInfos> at(size_t arrangerId, clk::Nanos position) const noexcept override;
    std::optional<pro::TimeInfos> within(size_t arrangerId, clk::Nanos start, clk::Nanos end) const noexcept override;

    void addObserver(ProjectObserver& observer) const noexcept override;
    void removeObserver(ProjectObserver& observer) noexcept override;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace engine
