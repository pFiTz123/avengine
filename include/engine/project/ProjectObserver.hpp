#pragma once

#include "reader/IReader.hpp"
#include <functional>
#include <memory>

namespace engine {
struct ProjectObserver {
    std::function<void(size_t, audio::ReaderPtr)> onEntryAdded;
    std::function<void(size_t)> onEntryRemoved;
};
;
} // namespace engine
