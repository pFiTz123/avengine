#pragma once

#include "ProjectObserver.hpp"
#include "project/IProject.hpp"
#include "reader/IReader.hpp"
#include "system/String.hpp"
#include "system/Time.hpp"
#include <functional>
#include <memory>
#include <optional>
#include <vector>

namespace engine {

using ReaderCreator = std::function<std::optional<audio::ReaderPtr>(const sys::String&)>;

struct IProjectController {
  public:
    virtual ~IProjectController() noexcept = default;
    virtual void init(const ReaderCreator& readerCreator) noexcept = 0;
    virtual size_t createArranger() noexcept = 0;
    virtual void setActive(size_t arrangerId) noexcept = 0;
    virtual void setActive(std::vector<size_t> arrangerIds) noexcept = 0;
    virtual void setInActive(size_t arrangerId) noexcept = 0;
    virtual void setInActive(std::vector<size_t> arrangerIds) noexcept = 0;
    virtual std::vector<size_t> getActive() const noexcept = 0;

    virtual std::optional<pro::Entry> add(size_t arrangerId,
                                          size_t trackIdx,
                                          sys::String file,
                                          clk::Nanos start,
                                          std::optional<clk::Nanos> end = std::nullopt) noexcept
        = 0;

    virtual void removeArranger(size_t arrangerId) noexcept = 0;
    virtual void removeTrack(size_t arrangerId, size_t trackIdx) noexcept = 0;
    virtual void removeEntry(size_t entryId) noexcept = 0;

    virtual void clear() noexcept = 0;
    virtual void clear(size_t arrangerId) noexcept = 0;
    virtual void clear(size_t arrangerId, size_t trackIdx) noexcept = 0;

    virtual clk::Nanos start() const noexcept = 0;
    virtual clk::Nanos start(size_t arrangerId) const noexcept = 0;
    virtual clk::Nanos start(size_t arrangerId, size_t trackIdx) const noexcept = 0;

    virtual clk::Nanos end() const noexcept = 0;
    virtual clk::Nanos end(size_t arrangerId) const noexcept = 0;
    virtual clk::Nanos end(size_t arrangerId, size_t trackIdx) const noexcept = 0;

    virtual clk::Nanos duration() const noexcept = 0;
    virtual std::optional<clk::Nanos> duration(size_t arrangerId) const noexcept = 0;
    virtual std::optional<clk::Nanos> duration(size_t arrangerId, size_t trackIdx) const noexcept = 0;

    virtual pro::TimeInfos at(clk::Nanos position) const noexcept = 0;
    virtual pro::TimeInfos within(clk::Nanos start, clk::Nanos end) const noexcept = 0;
    virtual std::optional<pro::TimeInfos> at(size_t arrangerId, clk::Nanos position) const noexcept = 0;
    virtual std::optional<pro::TimeInfos> within(size_t arrangerId, clk::Nanos start, clk::Nanos end) const noexcept
        = 0;

    virtual void addObserver(ProjectObserver& observer) const noexcept = 0;
    virtual void removeObserver(ProjectObserver& observer) noexcept = 0;
};
using ProjectControllerPtr = std::shared_ptr<IProjectController>;
} // namespace engine
