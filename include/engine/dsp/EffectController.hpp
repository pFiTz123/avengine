#pragma once

#include "IEffectController.hpp"

namespace engine {

class EffectController : public IEffectController {
  public:
    EffectController() noexcept;
    ~EffectController() noexcept;

    // Arranger
    virtual void addArrangerEffect(size_t arrangerId, audio::EffectPtr effect) noexcept override;
    virtual std::vector<audio::EffectPtr> getArrangerEffects(size_t arrangerId) const noexcept override;
    virtual void removeArrangerEffect(size_t arrangerId, size_t effectId) noexcept override;
    virtual void removeArrangerEffects(size_t arrangerId) noexcept override;

    // Track
    virtual void addTrackEffect(size_t arrangerId, size_t trackIdx, audio::EffectPtr effect) noexcept override;
    virtual std::vector<audio::EffectPtr> getTrackEffects(size_t arrangerId, size_t trackIdx) const noexcept override;
    virtual void removeTrackEffect(size_t arrangerId, size_t trackIdx, size_t effectId) noexcept override;
    virtual void removeTrackEffects(size_t arrangerId, size_t trackIdx) noexcept override;

    // Entry
    virtual void addEntryEffect(size_t entryId, audio::EffectPtr effect) noexcept override;
    virtual std::vector<audio::EffectPtr> getEntryEffects(size_t entryId) const noexcept override;
    virtual void removeEntryEffect(size_t entryId, size_t effectId) noexcept override;
    virtual void removeEntryEffects(size_t entryId) noexcept override;

    virtual void clear() noexcept override;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace engine
