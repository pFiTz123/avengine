#pragma once

#include "dsp/IEffect.hpp"
#include <memory>
#include <optional>
#include <vector>

namespace engine {

struct IEffectController {
    virtual ~IEffectController() noexcept = default;

    // Arranger
    virtual void addArrangerEffect(size_t arrangerId, audio::EffectPtr effect) noexcept = 0;
    virtual std::vector<audio::EffectPtr> getArrangerEffects(size_t arrangerId) const noexcept = 0;
    virtual void removeArrangerEffect(size_t arrangerId, size_t effectId) noexcept = 0;
    virtual void removeArrangerEffects(size_t arrangerId) noexcept = 0;

    // Track
    virtual void addTrackEffect(size_t arrangerId, size_t trackIdx, audio::EffectPtr effect) noexcept = 0;
    virtual std::vector<audio::EffectPtr> getTrackEffects(size_t arrangerId, size_t trackIdx) const noexcept = 0;
    virtual void removeTrackEffect(size_t arrangerId, size_t trackIdx, size_t effectId) noexcept = 0;
    virtual void removeTrackEffects(size_t arrangerId, size_t trackIdx) noexcept = 0;

    // Entry
    virtual void addEntryEffect(size_t entryId, audio::EffectPtr effect) noexcept = 0;
    virtual std::vector<audio::EffectPtr> getEntryEffects(size_t entryId) const noexcept = 0;
    virtual void removeEntryEffect(size_t entryId, size_t effectId) noexcept = 0;
    virtual void removeEntryEffects(size_t entryId) noexcept = 0;

    virtual void clear() noexcept = 0;
};
using EffectControllerPtr = std::shared_ptr<IEffectController>;
} // namespace engine
