#pragma once

#include "dsp/IEffectController.hpp"
#include "play/IPlayController.hpp"
#include "project/IProjectController.hpp"
#include "reader/IReaderController.hpp"
#include "reader/IReaderFactory.hpp"

namespace engine {

constexpr clk::Millis DEFAULT_REQUEST_DURATION{100};
constexpr audio::SampleFormat DEFAULT_SAMPLE_FORMAT = audio::SampleFormat(44100, 16, 2);

class Engine {
  public:
    Engine(ReaderControllerPtr, ProjectControllerPtr, PlayControllerPtr, EffectControllerPtr);
    Engine(const Engine&) = delete;
    ~Engine();

    ReaderControllerPtr readerController() const noexcept;
    ProjectControllerPtr projectController() const noexcept;
    PlayControllerPtr playController() const noexcept;
    EffectControllerPtr effectController() const noexcept;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
using EnginePtr = std::unique_ptr<Engine>;
} // namespace engine
