import os
import subprocess


def main():
    clang_format = "C:/Program Files/LLVM/bin/clang-format.exe"
    path = "{}/src".format(os.getcwd()).replace("\\", "/")
    module_path = "{}/modules".format(os.getcwd()).replace("\\", "/")
    FNULL = open(os.devnull, 'w')
    pattern = ".h", ".hpp", ".cpp"
    files = find_files(pattern, path)
    files.extend(find_files(pattern, module_path))
    for file in files:
        args = "{} -i -style=file {}".format(clang_format, file.replace("\\", "/"))
        print(args)
        subprocess.call(args, stdout=FNULL, stderr=FNULL, shell=False)


def find_files(patterns, path):
    files = []
    for root, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if filename.endswith(patterns):
                files.append(os.path.join(root, filename))
    return files


if __name__ == "__main__":
    main()
