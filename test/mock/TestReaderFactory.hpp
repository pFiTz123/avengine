#pragma once

#include "TestReader.hpp"
#include "reader/IReaderFactory.hpp"

namespace test {
struct TestReaderFactory : public audio::IReaderFactory {
    TestReaderFactory(size_t testSize, int16_t testValue)
      : testSize(testSize)
      , testValue(testValue)
    {
    }
    std::optional<audio::ReaderPtr> create(sys::String file, audio::SampleFormat fmt) noexcept override
    {
        auto reader = std::make_unique<TestReader>(testSize, testValue, fmt);
        reader->init(file);
        return reader;
    }

  private:
    size_t testSize;
    int16_t testValue;
};
} // namespace test
