#include "TestReader.hpp"
#include <atomic>

namespace {
std::atomic<size_t> nextId{0};
} // namespace

namespace test {
TestReader::TestReader(size_t testSize, int16_t testValue, audio::SampleFormat testFormat) noexcept
  : _id(++nextId)
  , testSize(testSize)
  , testValue(testValue)
  , testFormat(testFormat)
{
}

bool TestReader::init(sys::String string) noexcept
{
    file_ = string;
    return true;
}

size_t TestReader::id() const noexcept
{
    return _id;
}

const sys::String& TestReader::file() const noexcept
{
    return file_;
}

audio::Buffer TestReader::get() noexcept
{
    return get(clk::Seconds(1));
}

audio::Buffer TestReader::get(clk::Nanos clk) noexcept
{
    audio::Buffer data(testSize);
    std::fill(std::begin(data), std::end(data), testValue);
    return data;
}

void TestReader::seek(clk::Nanos clk) noexcept
{
}

clk::Nanos TestReader::position() const noexcept
{
    return clk::Nanos{0};
}

clk::Nanos TestReader::duration() const noexcept
{
    return clk::Seconds(10);
}

audio::SampleFormat TestReader::format() const noexcept
{
    return testFormat;
}

void TestReader::setFormat(const audio::SampleFormat& fmt) noexcept
{
    testFormat = fmt;
}
} // namespace test
