#pragma once

#include "reader/IReader.hpp"

namespace test {
class TestReader : public audio::IReader {
  public:
    TestReader(size_t testSize, int16_t testValue, audio::SampleFormat testFormat) noexcept;
    ~TestReader() = default;
    bool init(sys::String string) noexcept override;
    size_t id() const noexcept override;
    const sys::String& file() const noexcept override;
    audio::Buffer get() noexcept override;
    audio::Buffer get(clk::Nanos clk) noexcept override;
    void seek(clk::Nanos clk) noexcept override;
    clk::Nanos position() const noexcept override;
    clk::Nanos duration() const noexcept override;
    audio::SampleFormat format() const noexcept override;
    void setFormat(const audio::SampleFormat& fmt) noexcept override;

  private:
    size_t _id;
    size_t testSize;
    int16_t testValue;
    sys::String file_;
    audio::SampleFormat testFormat;
};
} // namespace test
