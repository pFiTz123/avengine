#include "engine/Engine.hpp"
#include "TestReaderFactory.hpp"
#include "dsp/EffectFactory.hpp"
#include "engine/dsp/EffectController.hpp"
#include "engine/play/PlayController.hpp"
#include "engine/project/ProjectController.hpp"
#include "engine/reader/ReaderController.hpp"
#include "reader/FFmpegReaderFactoryAdapter.hpp"
#include "system/Logger.hpp"
#include "system/Utility.hpp"
#include "test_config.hpp"
#include "gtest/gtest.h"
#include <fstream>
#include <ostream>
#include <thread>

using namespace test::data;
using namespace engine;

namespace {

EnginePtr createController(audio::SampleFormat format = DEFAULT_SAMPLE_FORMAT)
{
    ReaderControllerPtr readerController
        = std::make_shared<ReaderController>(std::make_unique<audio::FFmpegReaderFactoryAdapter>(), format);
    ProjectControllerPtr projectController = std::make_shared<ProjectController>();
    PlayControllerPtr playController = std::make_shared<PlayController>();
    EffectControllerPtr effectController = std::make_shared<EffectController>();
    return std::make_unique<Engine>(readerController, projectController, playController, effectController);
}

EnginePtr createControllerWithMockReader(size_t testSize = 10,
                                         int16_t testValue = 10,
                                         audio::SampleFormat format = DEFAULT_SAMPLE_FORMAT)
{
    ReaderControllerPtr readerController
        = std::make_shared<ReaderController>(std::make_unique<test::TestReaderFactory>(testSize, testValue), format);
    ProjectControllerPtr projectController = std::make_shared<ProjectController>();
    PlayControllerPtr playController = std::make_shared<PlayController>();
    EffectControllerPtr effectController = std::make_shared<EffectController>();
    return std::make_unique<Engine>(readerController, projectController, playController, effectController);
}

void writeRawFile(const std::string& filename, const audio::Buffer& buffer)
{
    std::ofstream stream(filename, std::ios::binary);
    stream.write((const char*)&buffer[0], buffer.size() * sizeof(int16_t));
    stream.close();
}
} // namespace

TEST(ControllerTest, FFmpegTest)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    auto one = ctrl->projectController()->add(arrangerId, 0, file::VOCALS_1, clk::Seconds(0));
    ASSERT_TRUE(one.has_value());
    auto playController = ctrl->playController();
    writeRawFile("Decode.raw", playController->request(playController->duration()));
}

TEST(ControllerTest, EntryEffect)
{
    auto ctrl = createControllerWithMockReader();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    auto one = ctrl->projectController()->add(arrangerId, 0, file::TEST, clk::Seconds(0), clk::Seconds(1));
    ASSERT_TRUE(one.has_value());
    auto playController = ctrl->playController();
    auto effectCtrl = ctrl->effectController();
    effectCtrl->addEntryEffect(one.value().id(), audio::EffectFactory::create<audio::Volume>(0.5f));
    auto buffer = playController->request(DEFAULT_REQUEST_DURATION);
    for (auto i : buffer) {
        EXPECT_EQ(5, i);
    }
}

TEST(ControllerTest, TrackEffect)
{
    auto ctrl = createControllerWithMockReader();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    auto one = ctrl->projectController()->add(arrangerId, 0, file::TEST, clk::Seconds(0), clk::Seconds(1));
    ASSERT_TRUE(one.has_value());
    auto playController = ctrl->playController();
    auto effectCtrl = ctrl->effectController();
    effectCtrl->addTrackEffect(arrangerId, 0, audio::EffectFactory::create<audio::Volume>(0.5f));
    auto buffer = playController->request(DEFAULT_REQUEST_DURATION);
    for (auto i : buffer) {
        EXPECT_EQ(5, i);
    }
}

TEST(ControllerTest, ArrangerEffect)
{
    auto ctrl = createControllerWithMockReader();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    auto one = ctrl->projectController()->add(arrangerId, 0, file::TEST, clk::Seconds(0), clk::Seconds(1));
    ASSERT_TRUE(one.has_value());
    auto playController = ctrl->playController();
    auto effectCtrl = ctrl->effectController();
    effectCtrl->addArrangerEffect(arrangerId, audio::EffectFactory::create<audio::Volume>(0.5f));
    auto buffer = playController->request(DEFAULT_REQUEST_DURATION);
    for (auto i : buffer) {
        EXPECT_EQ(5, i);
    }
}

TEST(ControllerTest, DecodeAllTestFiles)
{
    std::vector<sys::String> files{file::VOCALS_1, file::VOCALS_2, file::DRUMS, file::STRINGS, file::KEYS, file::BASS};
    size_t i = 0;
    for (const auto& file : files) {
        auto ctrl = createController();
        auto arrangerId = ctrl->projectController()->createArranger();
        ctrl->projectController()->setActive(arrangerId);
        auto one = ctrl->projectController()->add(arrangerId, 0, file, clk::Seconds(0));
        ASSERT_TRUE(one.has_value());
        auto buffer = ctrl->playController()->request(one.value().end());
        if (!buffer.empty())
            writeRawFile("file_" + std::to_string(i++) + ".raw", buffer);
    }
}

TEST(ControllerTest, AudioSink)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    std::vector<sys::String> files{file::VOCALS_1};

    for (size_t i = 0; i < files.size(); ++i) {
        auto val = ctrl->projectController()->add(arrangerId, i, files[i], clk::Seconds(0));
        ASSERT_TRUE(val.has_value());
    }
    auto playController = ctrl->playController();
    std::vector<audio::Buffer> buffers;
    playController->setAudioSink([&buffers, playController = playController.get()](auto&& buffer, auto pos, auto dur) {
        buffers.emplace_back(buffer);
    });
    playController->play();
    std::this_thread::sleep_for(playController->duration());
    playController->stop();
    auto buffer = audio::flatten(buffers);
    writeRawFile("AudioSink_Loop.raw", buffer);
}

TEST(ControllerTest, AudioSink_MultipleEntries)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    std::vector<sys::String> files{file::VOCALS_1, file::VOCALS_2, file::DRUMS, file::STRINGS, file::KEYS, file::BASS};
    size_t entriesPerTrack{5};
    for (size_t i = 0; i < files.size(); ++i) {
        clk::Nanos start = clk::Seconds(0);
        for (size_t j = 0; j < entriesPerTrack; ++j) {
            auto val = ctrl->projectController()->add(arrangerId, i, files[i], start);
            ASSERT_TRUE(val.has_value());
            start = val.value().end();
        }
    }
    auto playController = ctrl->playController();
    std::vector<audio::Buffer> buffers;
    playController->setAudioSink([&buffers, playController = playController.get()](auto&& buffer, auto pos, auto dur) {
        buffers.emplace_back(buffer);
    });
    playController->setLooping(true);
    playController->play();
    std::this_thread::sleep_for(clk::Seconds(10));
    playController->stop();
    auto buffer = audio::flatten(buffers);
    writeRawFile("AudioSink_MultipleEntries.raw", buffer);
}

TEST(ControllerTest, Mix)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    std::vector<sys::String> files{file::VOCALS_1, file::VOCALS_2, file::DRUMS, file::STRINGS, file::KEYS, file::BASS};

    for (size_t i = 0; i < files.size(); ++i) {
        auto val = ctrl->projectController()->add(arrangerId, i, files[i], clk::Seconds(0));
        ASSERT_TRUE(val.has_value());
    }
    auto playController = ctrl->playController();
    auto duration
        = static_cast<size_t>(std::round(clk::to_seconds(ctrl->projectController()->duration(arrangerId).value())));
    std::vector<audio::Buffer> buffers;
    for (size_t i = 0; i < duration; ++i) {
        buffers.emplace_back(playController->request(DEFAULT_REQUEST_DURATION));
    }
    auto buffer = audio::flatten(buffers);
    writeRawFile("Mix.raw", buffer);
}

TEST(ControllerTest, Loop)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    std::vector<sys::String> files{file::VOCALS_1, file::VOCALS_2};

    for (size_t i = 0; i < files.size(); ++i) {
        auto val = ctrl->projectController()->add(arrangerId, i, files[i], clk::Seconds(0), clk::Seconds(5));
        ASSERT_TRUE(val.has_value());
    }
    auto playController = ctrl->playController();
    playController->setLooping(true);
    std::vector<audio::Buffer> buffers;
    for (size_t i = 0; i < 10; ++i) {
        buffers.emplace_back(playController->request(DEFAULT_REQUEST_DURATION));
    }
    auto buffer = audio::flatten(buffers);
    writeRawFile("Loop.raw", buffer);
}

TEST(ControllerTest, Loop_Overread)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    std::vector<sys::String> files{file::VOCALS_1, file::VOCALS_2};

    for (size_t i = 0; i < files.size(); ++i) {
        auto val = ctrl->projectController()->add(arrangerId, i, files[i], clk::Seconds(0));
        ASSERT_TRUE(val.has_value());
    }
    auto playController = ctrl->playController();
    playController->setLooping(true);
    std::vector<audio::Buffer> buffers;
    for (size_t i = 0; i < 10; ++i) {
        buffers.emplace_back(playController->request(clk::Seconds(1)));
    }
    auto buffer = audio::flatten(buffers);
    writeRawFile("Loop_Overread.raw", buffer);
}

TEST(ControllerTest, ComplexArrangement)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    std::vector<sys::String> files{file::VOCALS_1, file::VOCALS_2, file::DRUMS, file::STRINGS, file::KEYS, file::BASS};
    size_t entriesPerTrack{5};
    for (size_t i = 0; i < files.size(); ++i) {
        clk::Nanos start = clk::Seconds(0);
        for (size_t j = 0; j < entriesPerTrack; ++j) {
            auto val = ctrl->projectController()->add(arrangerId, i, files[i], start);
            ASSERT_TRUE(val.has_value());
            start = val.value().end();
        }
    }
    auto playController = ctrl->playController();
    playController->setLooping(true);
    auto duration
        = static_cast<size_t>(std::round(clk::to_seconds(ctrl->projectController()->duration(arrangerId).value())));
    std::vector<audio::Buffer> buffers;
    for (size_t i = 0; i < duration * 2; ++i) {
        buffers.emplace_back(playController->request(DEFAULT_REQUEST_DURATION));
    }
    auto buffer = audio::flatten(buffers);
    writeRawFile("ComplexArrangement.raw", buffer);
}

TEST(ControllerTest, MixArrangement)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    std::vector<sys::String> files{file::VOCALS_1, file::VOCALS_2, file::DRUMS, file::STRINGS, file::KEYS, file::BASS};
    size_t i = 0;
    std::vector<audio::Buffer> buffers;
    for (const auto& file : files) {
        auto one = ctrl->projectController()->add(arrangerId, i++, file, clk::Seconds(0));
        ASSERT_TRUE(one.has_value());
    }
    auto buffer = ctrl->playController()->request(ctrl->playController()->duration());
    writeRawFile("MixArrangement.raw", buffer);
}

TEST(ControllerTest, MultipleEntries)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    std::vector<sys::String> files{file::VOCALS_1};
    size_t entriesPerTrack{5};
    for (size_t i = 0; i < files.size(); ++i) {
        clk::Nanos start = clk::Seconds(0);
        for (size_t j = 0; j < entriesPerTrack; ++j) {
            auto val = ctrl->projectController()->add(arrangerId, i, files[i], start);
            ASSERT_TRUE(val.has_value());
            start = val.value().end();
        }
    }
    auto playController = ctrl->playController();
    playController->setLooping(true);
    auto duration
        = static_cast<size_t>(std::round(clk::to_seconds(ctrl->projectController()->duration(arrangerId).value())));
    std::vector<audio::Buffer> buffers;
    for (size_t i = 0; i < duration * 2; ++i) {
        buffers.emplace_back(playController->request(DEFAULT_REQUEST_DURATION));
    }
    auto buffer = audio::flatten(buffers);
    writeRawFile("MultipleEntries.raw", buffer);
}

TEST(ControllerTest, Playing)
{
    auto ctrl = createController();
    auto arrangerId = ctrl->projectController()->createArranger();
    ctrl->projectController()->setActive(arrangerId);
    auto one = ctrl->projectController()->add(arrangerId, 0, file::TEST, clk::Seconds(0));
    ASSERT_TRUE(one.has_value());
    auto playController = ctrl->playController();
    playController->play();
    EXPECT_TRUE(playController->isPlaying());
    playController->stop();
    EXPECT_FALSE(playController->isPlaying());
}
