[settings]
compiler=clang
compiler.libcxx=libc++
compiler.version=8
os=Android
os.api_level=@API@
arch=@ARCH@
os_build=@HOST_SYSTEM@
arch_build=@HOST_ARCH@
build_type=@BUILD_TYPE@

[build_requires]
*: android-ndk-r19b/1.1@magix/testing