package com.magix.android.engine

import android.annotation.TargetApi
import android.media.AudioDeviceInfo
import android.media.AudioManager
import java.util.Vector

/**
 * POJO which represents basic information for an audio device.
 *
 * Example: id: 8, deviceName: "built-in speaker"
 */
class AudioDeviceEntry(val id: Int, val name: String?) {

    override fun toString(): String {
        return name ?: ""
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val that = other as AudioDeviceEntry?

        if (id != that!!.id) return false
        return if (name != null) name == that.name else that.name == null
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + (name?.hashCode() ?: 0)
        return result
    }

    companion object {

        /**
         * Create a list of AudioDeviceListEntry objects from a list of AudioDeviceInfo objects.
         *
         * @param devices A list of {@Link AudioDeviceInfo} objects
         * @param directionType Only audio devices with this direction will be included in the list.
         * Valid values are GET_DEVICES_ALL, GET_DEVICES_OUTPUTS and
         * GET_DEVICES_INPUTS.
         * @return A list of AudioDeviceListEntry objects
         */
        @TargetApi(23)
        internal fun createListFrom(devices: Array<AudioDeviceInfo>, directionType: Int): List<AudioDeviceEntry> {

            val listEntries = Vector<AudioDeviceEntry>()
            for (info in devices) {
                if (directionType == AudioManager.GET_DEVICES_ALL ||
                        directionType == AudioManager.GET_DEVICES_OUTPUTS && info.isSink ||
                        directionType == AudioManager.GET_DEVICES_INPUTS && info.isSource) {
                    listEntries.add(AudioDeviceEntry(info.id, info.productName.toString() + " " +
                            AudioDeviceInfoConverter.typeToString(info.type)))
                }
            }
            return listEntries
        }
    }
}