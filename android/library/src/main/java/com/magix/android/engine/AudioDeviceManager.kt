package com.magix.android.engine

import android.annotation.TargetApi
import android.content.Context
import android.media.AudioDeviceCallback
import android.media.AudioDeviceInfo
import android.media.AudioManager
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class AudioDeviceManager(context: Context) {
    private val audioManager: AudioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    private var directionType: Int = AudioManager.GET_DEVICES_OUTPUTS
    private val deviceListSubject = PublishSubject.create<ArrayList<AudioDeviceEntry>>()
    val deviceList: ArrayList<AudioDeviceEntry> = ArrayList()
    val deviceListObservable = deviceListSubject as Observable<ArrayList<AudioDeviceEntry>>

    @TargetApi(23)
    fun setDirectionType(directionType: Int) {
        this.directionType = directionType
        setupAudioDeviceCallback()
    }

    private fun setupAudioDeviceCallback() {
        audioManager.registerAudioDeviceCallback(object : AudioDeviceCallback() {
            override fun onAudioDevicesAdded(addedDevices: Array<AudioDeviceInfo>) {

                val devices = AudioDeviceEntry.createListFrom(addedDevices, directionType)
                if (devices.isNotEmpty()) {
                    deviceList.addAll(devices)
                    deviceListSubject.onNext(deviceList)
                }
            }

            override fun onAudioDevicesRemoved(removedDevices: Array<AudioDeviceInfo>) {

                val devices = AudioDeviceEntry.createListFrom(removedDevices, directionType)
                for (entry in devices) {
                    deviceList.remove(entry)
                    deviceListSubject.onNext(deviceList)
                }
            }
        }, null)
    }
}
