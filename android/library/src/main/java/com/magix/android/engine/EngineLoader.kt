package com.magix.android.engine

object EngineLoader {

    fun load() {
        System.loadLibrary("c++_shared") //NON-NLS
        System.loadLibrary("engine-jni") //NON-NLS
    }
}