package com.magix.android.engine

import com.magix.android.engine.generated.AbstractEngineBridge
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

val AbstractEngineBridge.positionObservable: Observable<Float>
    get() {
        return Observable
            .interval(25, TimeUnit.MILLISECONDS)
            .map { position().toFloat() / end().toFloat() }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
    }