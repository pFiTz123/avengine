#include "Stream.hpp"
#include "engine/Engine.hpp"
#include "system/Logger.hpp"
#include "system/Utility.hpp"
#include <algorithm>
#include <cinttypes>
#include <cmath>
#include <memory>
#include <utility>

namespace {
constexpr int64_t kNanosPerMillisecond = 1'000'000; // Use int64_t to avoid overflows in calculations
size_t toFrames(const audio::SampleFormat &format, clk::Nanos duration)
{
    return audio::toSamples(format.sampleRate, format.channels, duration) / format.channels;
}

clk::Nanos toNanos(const audio::SampleFormat &format, int32_t numFrames)
{
    return clk::from_seconds<clk::Nanos>(
        audio::toSeconds(static_cast<size_t>(numFrames * format.channels), format.sampleRate, format.channels));
}
} // namespace

Stream::Stream(RequestFunc requester, const audio::SampleFormat &format) noexcept
  : mRequester(std::move(requester))
  , mFormat(format)
{
    createPlaybackStream();
}

Stream::~Stream() noexcept
{
    closeOutputStream();
}

void Stream::setDeviceId(int32_t deviceId) noexcept
{
    mPlaybackDeviceId = deviceId;

    int32_t currentDeviceId = mPlayStream->getDeviceId();
    if (deviceId != currentDeviceId)
        restart();
}

void Stream::createPlaybackStream() noexcept
{
    oboe::AudioStreamBuilder builder;
    setupPlaybackStreamParameters(&builder);

    oboe::Result result = builder.openStream(&mPlayStream);
    if (result == oboe::Result::OK && mPlayStream != nullptr) {
        int channelCount = mPlayStream->getChannelCount();
        if (channelCount != mFormat.channels) {
            LOGWV("Requested %d channels but received %d", mFormat.channels, channelCount);
        }
    }
    else {
        LOGEV("Failed to create stream. Error: %s", oboe::convertToText(result));
    }
}

void Stream::setupPlaybackStreamParameters(oboe::AudioStreamBuilder *builder) noexcept
{
    builder->setAudioApi(mAudioApi);
    builder->setDeviceId(mPlaybackDeviceId);
    builder->setChannelCount(mFormat.channels);
    builder->setFormat(oboe::AudioFormat::I16);
    builder->setCallback(this);
    // builder->setBufferCapacityInFrames(toFrames(mFormat, engine::DEFAULT_REQUEST_DURATION) * 2);
    builder->setSampleRate(mFormat.sampleRate);
    builder->setSharingMode(oboe::SharingMode::Exclusive);
    builder->setPerformanceMode(oboe::PerformanceMode::LowLatency);
}

void Stream::closeOutputStream() noexcept
{
    if (mPlayStream != nullptr) {
        oboe::Result result = mPlayStream->requestStop();
        if (result != oboe::Result::OK) {
            LOGEV("Error stopping output stream. %s", oboe::convertToText(result));
        }

        result = mPlayStream->close();
        if (result != oboe::Result::OK) {
            LOGEV("Error closing output stream. %s", oboe::convertToText(result));
        }
    }
}

void Stream::restart() noexcept
{
    if (mRestartingLock.try_lock()) {
        closeOutputStream();
        createPlaybackStream();
        mRestartingLock.unlock();
    }
}

void Stream::setAudioApi(oboe::AudioApi audioApi) noexcept
{
    if (audioApi != mAudioApi) {
        LOGDV("Setting Audio API to %s", oboe::convertToText(audioApi));
        mAudioApi = audioApi;
        restart();
    }
    else {
        LOGWV("Audio API was already set to %s, not setting", oboe::convertToText(audioApi));
    }
}

oboe::DataCallbackResult Stream::onAudioReady(oboe::AudioStream *audioStream, void *audioData, int32_t numFrames)
{
    auto buffer = mRequester(toNanos(mFormat, numFrames));
    memcpy(audioData, buffer.data(), buffer.size() * sizeof(audio::Buffer::value_type));
    return oboe::DataCallbackResult::Continue;
}

// void Stream::onAudioBufferAvailable(const audio::Buffer &buffer, clk::Nanos pos, clk::Nanos dur) noexcept
//{
//    auto numFrames = static_cast<int32_t>(toFrames(mFormat, dur));
//    auto result = mPlayStream->write(buffer.data(), numFrames, 100 * kNanosPerMillisecond);
//    if (result) {
//        if (result.value() != numFrames) {
//            LOGWV("Did not write full buffer size, size: %d, written: %d", numFrames, result.value());
//        }
//    }
//    else {
//        LOGEV("Error writing buffer %s", oboe::convertToText(result.error()));
//    }
//}

void Stream::play() noexcept
{
    auto streamState = mPlayStream->getState();
    if (streamState == oboe::StreamState::Starting || streamState == oboe::StreamState::Started)
        return;
    auto result = mPlayStream->requestStart();
    if (result != oboe::Result::OK) {
        LOGEV("Error starting stream. %s", oboe::convertToText(result));
    }
}

void Stream::pause() noexcept
{
    auto streamState = mPlayStream->getState();
    if (streamState == oboe::StreamState::Pausing || streamState == oboe::StreamState::Paused)
        return;
    auto result = mPlayStream->requestPause();
    if (result != oboe::Result::OK) {
        LOGEV("Error pausing stream. %s", oboe::convertToText(result));
    }
}

void Stream::stop() noexcept
{
    auto streamState = mPlayStream->getState();
    if (streamState == oboe::StreamState::Stopping || streamState == oboe::StreamState::Stopped)
        return;
    auto result = mPlayStream->requestStop();
    if (result != oboe::Result::OK) {
        LOGEV("Error stopping stream. %s", oboe::convertToText(result));
    }
}

void Stream::flush() noexcept
{
    auto result = mPlayStream->requestFlush();
    if (result != oboe::Result::OK) {
        LOGEV("Error flushing stream. %s", oboe::convertToText(result));
    }
}

void Stream::onErrorAfterClose(oboe::AudioStream *oboeStream, oboe::Result error)
{
    if (error == oboe::Result::ErrorDisconnected)
        restart();
}
