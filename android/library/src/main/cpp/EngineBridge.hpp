#include "AbstractEngineBridge.hpp"
#include "Entry.hpp"

class EngineBridge : public gen::AbstractEngineBridge {
  public:
    EngineBridge(const gen::SampleFormat& sampleFormat);
    ~EngineBridge() override;

    int64_t createArranger() override;
    void setActiveArranger(int64_t arrangerId) override;
    void setActiveArrangers(const std::vector<int64_t>& arrangerIds) override;
    std::vector<int64_t> getActiveArrangers() const override;
    void setInActiveArranger(int64_t arrangerId) override;
    void setInActiveArrangers(const std::vector<int64_t>& arrangerIds) override;

    std::optional<gen::Entry> add(int64_t arrangerId,
                                  int64_t trackIdx,
                                  const std::string& file,
                                  int64_t start,
                                  std::optional<int64_t> end,
                                  std::optional<int64_t> streamOffset) override;
    void remove(int64_t entryId) override;
    void play(bool loop) override;
    void pause() override;
    void stop() override;
    void seek(int64_t position) override;
    void setOutputDevice(int32_t id) override;
    int64_t start() const override;
    int64_t end() const override;
    int64_t position() const override;
    int64_t duration() const override;
    gen::SampleFormat format() const override;
    bool isPlaying() const override;
    void reset() override;
    void setEntryVolume(int64_t entryId, float volume) override;
    void setTrackVolume(int64_t arrangerId, int64_t trackIdx, float volume) override;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
