#pragma once

#include "system/Buffer.hpp"
#include "system/SampleFormat.hpp"
#include "system/Time.hpp"
#include <functional>
#include <oboe/Oboe.h>
#include <thread>

using RequestFunc = std::function<audio::Buffer(clk::Nanos)>;

class Stream : oboe::AudioStreamCallback {
  public:
    Stream(RequestFunc requester, const audio::SampleFormat &sampleFormat) noexcept;
    ~Stream() noexcept;
    // void onAudioBufferAvailable(const audio::Buffer& buffer, clk::Nanos pos, clk::Nanos dur) noexcept;
    void setAudioApi(oboe::AudioApi audioApi) noexcept;
    void setDeviceId(int32_t deviceId) noexcept;
    void play() noexcept;
    void pause() noexcept;
    void stop() noexcept;
    void flush() noexcept;
    void restart() noexcept;

    oboe::DataCallbackResult onAudioReady(oboe::AudioStream *audioStream, void *audioData, int32_t numFrames);
    void onErrorAfterClose(oboe::AudioStream *oboeStream, oboe::Result error);

  private:
    oboe::AudioApi mAudioApi = oboe::AudioApi::Unspecified;
    int32_t mPlaybackDeviceId = oboe::kUnspecified;
    RequestFunc mRequester;
    audio::SampleFormat mFormat;
    oboe::AudioStream *mPlayStream = nullptr;
    std::mutex mRestartingLock;
    void createPlaybackStream() noexcept;
    void closeOutputStream() noexcept;
    void setupPlaybackStreamParameters(oboe::AudioStreamBuilder *builder) noexcept;
};
