#include "EngineBridge.hpp"
#include "Converter.hpp"
#include "Stream.hpp"
#include "dsp/EffectFactory.hpp"
#include "engine/Engine.hpp"
#include "engine/dsp/EffectController.hpp"
#include "engine/play/PlayController.hpp"
#include "engine/project/ProjectController.hpp"
#include "engine/reader/ReaderController.hpp"
#include "reader/FFmpegReaderFactoryAdapter.hpp"
#include <memory>

using namespace engine;

struct EngineBridge::pImpl {
    explicit pImpl(audio::SampleFormat&& sampleFormat)
      : readerController(
            std::make_shared<ReaderController>(std::make_unique<audio::FFmpegReaderFactoryAdapter>(), sampleFormat))
      , engine(std::make_unique<Engine>(readerController, projectController, playController, effectController))
      , stream([ctrl = playController.get()](clk::Nanos dur) { return ctrl->request(dur); }, sampleFormat)
    {
        // playController->setAudioSink([& stream = stream](const audio::Buffer& buffer, auto pos, auto dur) {
        //    stream.onAudioBufferAvailable(buffer, pos, dur);
        //});
    }

    ReaderControllerPtr readerController = nullptr;
    ProjectControllerPtr projectController = std::make_shared<ProjectController>();
    PlayControllerPtr playController = std::make_shared<PlayController>();
    EffectControllerPtr effectController = std::make_shared<EffectController>();
    EnginePtr engine = nullptr;
    Stream stream;
};

EngineBridge::EngineBridge(const gen::SampleFormat& sampleFormat)
  : p(std::make_unique<pImpl>(convertSampleFormat(sampleFormat)))
{
}

EngineBridge::~EngineBridge()
{
}

std::optional<gen::Entry> EngineBridge::add(int64_t arrangerId,

                                            int64_t trackIdx,
                                            const std::string& file,
                                            int64_t start,
                                            std::optional<int64_t> end,
                                            std::optional<int64_t> streamOffset)
{
    std::optional<gen::Entry> e;
    if (auto entry = p->projectController->add(static_cast<size_t>(arrangerId),
                                               static_cast<size_t>(trackIdx),
                                               file,
                                               clk::Nanos{start},
                                               end ? clk::Nanos{end.value()} : std::optional<clk::Nanos>())) {
        if (streamOffset) {
            if (auto reader = p->readerController->getReader(entry.value().id())) {
                p->readerController->setOffset(reader.value()->id(), clk::Nanos{streamOffset.value()});
            }
        }
        e = convertEntry(entry.value());
    }
    return e;
}

void EngineBridge::remove(int64_t entryId)
{
    p->projectController->removeEntry(static_cast<size_t>(entryId));
}

void EngineBridge::play(bool loop)
{
    p->playController->setLooping(loop);
    p->playController->play();
    p->stream.play();
}

void EngineBridge::pause()
{
    p->playController->pause();
    p->stream.pause();
}

void EngineBridge::stop()
{
    p->playController->stop();
    p->stream.stop();
}

void EngineBridge::seek(int64_t position)
{
    p->stream.pause();
    p->playController->seek(clk::Nanos{position});
    p->stream.play();
}

int64_t EngineBridge::start() const
{
    return p->playController->start().count();
}

int64_t EngineBridge::end() const
{
    return p->playController->end().count();
}

int64_t EngineBridge::position() const
{
    return p->playController->position().count();
}

int64_t EngineBridge::duration() const
{
    return p->playController->duration().count();
}

gen::SampleFormat EngineBridge::format() const
{
    return convertSampleFormat(p->readerController->sampleFormat());
}

void EngineBridge::setOutputDevice(int32_t id)
{
    p->stream.setDeviceId(id);
}

bool EngineBridge::isPlaying() const
{
    return p->playController->isPlaying();
}

void EngineBridge::reset()
{
    p->stream.stop();
    p->projectController->clear();
}

void EngineBridge::setEntryVolume(int64_t entryId, float volume)
{
    p->effectController->removeEntryEffects(static_cast<size_t>(entryId));
    if (volume < 1.f)
        p->effectController->addEntryEffect(static_cast<size_t>(entryId),
                                            audio::EffectFactory::create<audio::Volume>(volume));
}

void EngineBridge::setTrackVolume(int64_t arrangerId, int64_t trackIdx, float volume)
{
    p->effectController->removeTrackEffects(static_cast<size_t>(arrangerId), static_cast<size_t>(trackIdx));
    if (volume < 1.f)
        p->effectController->addTrackEffect(static_cast<size_t>(arrangerId),
                                            static_cast<size_t>(trackIdx),
                                            audio::EffectFactory::create<audio::Volume>(volume));
}

int64_t EngineBridge::createArranger()
{
    return static_cast<int64_t>(p->projectController->createArranger());
}

void EngineBridge::setActiveArranger(int64_t arrangerId)
{
    p->projectController->setActive(static_cast<size_t>(arrangerId));
}

void EngineBridge::setActiveArrangers(const std::vector<int64_t>& arrangerIds)
{
    p->projectController->setActive(std::vector<size_t>(std::begin(arrangerIds), std::end(arrangerIds)));
}

std::vector<int64_t> EngineBridge::getActiveArrangers() const
{
    const auto& active = p->projectController->getActive();
    return std::vector<int64_t>(std::begin(active), std::end(active));
}

void EngineBridge::setInActiveArranger(int64_t arrangerId)
{
    p->projectController->setInActive(static_cast<size_t>(arrangerId));
}

void EngineBridge::setInActiveArrangers(const std::vector<int64_t>& arrangerIds)
{
    p->projectController->setInActive(std::vector<size_t>(std::begin(arrangerIds), std::end(arrangerIds)));
}

std::shared_ptr<gen::AbstractEngineBridge> gen::AbstractEngineBridge::create(const gen::SampleFormat& sampleFormat)
{
    return std::make_shared<EngineBridge>(sampleFormat);
}
