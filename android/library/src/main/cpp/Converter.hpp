#pragma once

#include "Entry.hpp"
#include "SampleFormat.hpp"
#include "system/SampleFormat.hpp"
#include "project/Entry.hpp"

audio::SampleFormat convertSampleFormat(const gen::SampleFormat& format);
gen::SampleFormat convertSampleFormat(const audio::SampleFormat& format);
gen::Entry convertEntry(const pro::Entry& entry);
