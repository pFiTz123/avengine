#include "Converter.hpp"

audio::SampleFormat convertSampleFormat(const gen::SampleFormat& format) {
    return audio::SampleFormat(static_cast<audio::Samplerate>(format.sampleRate),
                               static_cast<audio::BitsPerSample>(format.bitsPerSample),
                               static_cast<audio::Channels>(format.channelCount));
}
gen::SampleFormat convertSampleFormat(const audio::SampleFormat& format) {
    return gen::SampleFormat(format.sampleRate, format.channels, format.bitsPerSample);
}
gen::Entry convertEntry(const pro::Entry& entry) {
    return gen::Entry(entry.id(), entry.start().count(), entry.end().count());
}
