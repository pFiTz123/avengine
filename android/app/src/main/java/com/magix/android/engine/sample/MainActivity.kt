package com.magix.android.engine.sample

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioManager
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.magix.android.engine.AudioDeviceManager
import com.magix.android.engine.EngineLoader
import com.magix.android.engine.generated.AbstractEngineBridge
import com.magix.android.engine.generated.SampleFormat
import com.magix.android.engine.sample.tools.AudioPicker
import com.magix.android.engine.sample.tools.UriCopier
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.io.File

class MainActivity : AppCompatActivity() {

    private val PERMISSION_REQUEST: Int = 100
    private lateinit var engineBridge: AbstractEngineBridge
    private val sampleFormat = SampleFormat(48000, 2, 16)
    private val disposables = CompositeDisposable()
    private lateinit var audioDeviceManager: AudioDeviceManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Timber.plant(Timber.DebugTree())
        EngineLoader.load()
        main_play.setOnClickListener { play() }
        main_stop.setOnClickListener { stop() }
        engineBridge = AbstractEngineBridge.create(sampleFormat)!!
        audioDeviceManager = AudioDeviceManager(this)
        audioDeviceManager.setDirectionType(AudioManager.GET_DEVICES_OUTPUTS)
        disposables.add(audioDeviceManager.deviceListObservable.subscribe {
            it.forEachIndexed { index, audioDeviceEntry ->
                Timber.i("Device ($index) Id ${audioDeviceEntry.id} Name ${audioDeviceEntry.name}")
            }
            engineBridge.setOutputDevice(it[1].id)
        })

        main_pick_audio.setOnClickListener {
            if (havePermissions())
                AudioPicker.startIntent(this)
            else
                requestPermissions()
        }
    }

    private fun play() {
        engineBridge.play(true)
    }

    private fun stop() {
        engineBridge.stop()
    }

    private fun havePermissions(): Boolean {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        AudioPicker.handleResult(resultCode, data)?.let { onAudioAvailable(it) }
    }

    private fun onAudioAvailable(uri: Uri) {
        Toast.makeText(this, "audio file picked $uri", Toast.LENGTH_LONG).show()
        val cacheFile = File(externalCacheDir, uri.hashCode().toString() + ".mp3")
        UriCopier.copyUriToTarget(this, uri, cacheFile.absolutePath)
        val active = engineBridge.activeArrangers
        engineBridge.setInActiveArrangers(active)
        val arrangerId = engineBridge.createArranger()
        engineBridge.setActiveArranger(arrangerId)
        val entry = engineBridge.add(arrangerId, 0, cacheFile.absolutePath, 0, null, null)
        if (entry == null)
            Toast.makeText(this, "Failed to import file!", Toast.LENGTH_LONG).show()
        else
            Timber.i("Imported file with start=${entry.start}, end=${entry.end}")
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    //requestPermissions()
                }
                return
            }
        }
    }

    private fun requestPermissions() {
        if (havePermissions()) return
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

    override fun onPause() {
        super.onPause()
        stop()
    }
}
