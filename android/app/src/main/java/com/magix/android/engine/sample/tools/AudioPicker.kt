package com.magix.android.engine.sample.tools

import android.app.Activity
import android.content.Intent
import android.net.Uri
import timber.log.Timber

object AudioPicker {

    private const val REQUEST_PICK_AUDIO = 1

    fun startIntent(activity: Activity) {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "audio/*"
        activity.startActivityForResult(intent, REQUEST_PICK_AUDIO)
    }

    fun handleResult(resultCode: Int, data: Intent?): Uri? {
        if (resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val audioUri = data.data
            Timber.i("audio file picked $audioUri")
            return audioUri
        }

        return null
    }
}