package com.magix.android.engine.sample.tools

import android.content.Context
import android.net.Uri
import timber.log.Timber
import java.io.*

object UriCopier {

    fun copyUriToTarget(context: Context, uri: Uri, target: String): File {
        val inStream: InputStream = if (uri.path!!.contains("/android_asset/")) {
            context.assets.open(uri.path!!.removePrefix("/android_asset/"))
        } else {
            context.contentResolver.openInputStream(uri)!!
        }

        val targetFile = File(target)

        if (!targetFile.parentFile.exists()) {
            targetFile.parentFile.mkdirs()
        }

        var bis: BufferedInputStream? = null
        var bos: BufferedOutputStream? = null
        try {
            bis = BufferedInputStream(inStream)
            bos = BufferedOutputStream(FileOutputStream(targetFile, false))
            val buf = ByteArray(1024)
            bis.read(buf)
            do {
                bos.write(buf)
            } while (bis.read(buf) !== -1)
        } catch (e: IOException) {
            Timber.e(e)
        } finally {
            try {
                bis?.close()
                bos?.close()
            } catch (e: IOException) {
                Timber.e(e)
            }
        }

        return targetFile
    }
}