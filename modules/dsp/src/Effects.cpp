#include "dsp/Effects.hpp"
#include "config.hpp"

namespace audio {
void Volume::apply(audio::Buffer& buffer, float volume)
{
    for (auto& i : buffer)
        i *= volume;
}
} // namespace audio
