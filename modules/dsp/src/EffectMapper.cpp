#include "dsp/EffectMapper.hpp"
#include "dsp/EffectFactory.hpp"
#include "system/Mutex.hpp"
#include <algorithm>
#include <vector>

namespace audio {

struct EffectMapper::pImpl {
    pImpl() = default;
    Mutex mutex;
    std::vector<size_t> ids;
    std::vector<std::vector<audio::EffectPtr>> effects;
};

EffectMapper::EffectMapper() noexcept
  : p(std::make_unique<pImpl>())
{
}

EffectMapper::~EffectMapper() noexcept
{
}

void EffectMapper::add(size_t id, audio::EffectPtr effect) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found == std::end(p->ids)) {
        p->ids.emplace_back(id);
        p->effects.emplace_back(std::vector<audio::EffectPtr>{effect});
    }
    else {
        auto idx = std::distance(std::begin(p->ids), found);
        auto& idEffects = p->effects.at(idx);
        auto effectFound
            = std::find_if(std::begin(idEffects), std::end(idEffects), [effectId = effect->id()](const auto& effect) {
                  return effectId == effect->id();
              });
        if (effectFound == std::end(idEffects)) {
            idEffects.emplace_back(effect);
        }
        else {
            *effectFound = effect;
        }
    }
}

std::vector<audio::EffectPtr> EffectMapper::get(size_t id) const noexcept
{
    std::vector<audio::EffectPtr> effect;
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        effect = p->effects.at(idx);
    }
    return effect;
}

void EffectMapper::remove(size_t id, size_t effectId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        auto& idEffects = p->effects.at(idx);
        auto effectFound = std::find_if(std::begin(idEffects), std::end(idEffects), [effectId](const auto& effect) {
            return effectId == effect->id();
        });
        if (effectFound != std::end(idEffects)) {
            idEffects.erase(effectFound);
            if (idEffects.empty()) {
                p->ids.erase(found);
                p->effects.erase(std::begin(p->effects) + idx);
            }
        }
    }
}

void EffectMapper::removeAll(size_t id) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        p->ids.erase(found);
        p->effects.erase(std::begin(p->effects) + idx);
    }
}

void EffectMapper::clear() noexcept
{
    ScopedLock lck(p->mutex);
    p->ids.clear();
    p->effects.clear();
}
} // namespace audio
