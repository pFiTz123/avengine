#include "dsp/Mix.hpp"

namespace audio {
[[nodiscard]] audio::Buffer mix(std::vector<audio::Buffer>&& buffers)
{
    std::sort(std::begin(buffers), std::end(buffers), [](const auto& a, const auto& b) { return a.size() > b.size(); });
    auto result = buffers.front();
    for (auto& i : result) {
        i *= 1. / buffers.size();
    }
    for (auto it = std::next(std::begin(buffers)); it != buffers.end(); ++it) {
        auto& buf = (*it);
        auto oldSize = buf.size();
        if (oldSize < result.size()) {
            buf.resize(result.size());
            std::fill(std::begin(buf) + oldSize, std::end(buf), 0);
        }
        std::transform(std::begin(result),
                       std::end(result),
                       std::begin(*it),
                       std::begin(result),
                       [size = buffers.size()](auto a, auto b) {
                           b *= 1. / size;
                           return audio::alg1::mixSamples(a, b);
                       });
    }
    using limit = std::numeric_limits<int16_t>;
    for (auto& i : result) {
        int32_t a = i * static_cast<int32_t>(buffers.size());
        if (limit::max() < a)
            i = limit::max();
        else if (limit::min() > a)
            i = limit::min();
        else
            i = a;
    }
    return result;
}
namespace alg3 {

[[nodiscard]] int16_t mixSamples(int16_t l, int16_t r) noexcept
{
    int32_t m; // mixed result will go here

    using limit_16bit = std::numeric_limits<int16_t>;
    using limit_32bit = std::numeric_limits<int32_t>;
    auto max_16bit = limit_16bit::max();
    auto max_32bit = limit_32bit::max();

    // Make both samples unsigned (0..65535)
    int32_t a = l + 32768;
    int32_t b = r + 32768;

    // Pick the equation
    if ((a < 32768) || (b < 32768)) {
        // Viktor's first equation when both sources are "quiet"
        // (i.e. less than middle of the dynamic range)
        m = a * b / 32768;
    }
    else {
        // Viktor's second equation when one or both sources are loud
        m = 2 * (a + b) - (a * b) / 32768 - 65536;
    }

    // Output is unsigned (0..65536) so convert back to signed (-32768..32767)
    if (m == 65536)
        m = 65535;
    m -= 32768;
    return m;
}
} // namespace alg3
} // namespace audio
