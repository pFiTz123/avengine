#pragma once

#include "system/Buffer.hpp"
#include <atomic>
#include <memory>

namespace {
std::atomic<size_t> nextId{0};
}

namespace audio {
struct IEffect {
    virtual ~IEffect() = default;
    virtual size_t id() const noexcept
    {
        return id_;
    }
    virtual void apply(audio::Buffer& buffer) const noexcept = 0;

  private:
    size_t id_{++nextId};
};
using EffectPtr = std::shared_ptr<IEffect>;
} // namespace audio
