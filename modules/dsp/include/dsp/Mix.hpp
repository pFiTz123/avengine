#pragma once

#include "system/Buffer.hpp"
#include <algorithm>
#include <limits>
#include <vector>

namespace audio {
namespace alg1 {
inline int16_t mixSamples(int16_t sample1, int16_t sample2)
{
    const int32_t result(static_cast<int32_t>(sample1) + static_cast<int32_t>(sample2));
    typedef std::numeric_limits<int16_t> Range;
    if (Range::max() < result)
        return Range::max();
    else if (Range::min() > result)
        return Range::min();
    else
        return result;
}
} // namespace alg1
namespace alg2 {
[[nodiscard]] inline int16_t mixSamples(int16_t a, int16_t b) noexcept
{
    using limit = std::numeric_limits<int16_t>;
    return a < 0 && b < 0
               ? ((int32_t)a + (int32_t)b) - (((int32_t)a * (int32_t)b) / limit::min())
               : (a > 0 && b > 0 ? ((int32_t)a + (int32_t)b) - (((int32_t)a * (int32_t)b) / limit::max()) : a + b);
};
} // namespace alg2
namespace alg3 {

[[nodiscard]] int16_t mixSamples(int16_t l, int16_t r) noexcept;
} // namespace alg3

[[nodiscard]] audio::Buffer mix(std::vector<audio::Buffer>&& buffers);
} // namespace audio
