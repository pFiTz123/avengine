#pragma once

#include "system/Buffer.hpp"
#include <functional>

#define DECLARE_EFFECT(name, ...)                                           \
    struct name {                                                           \
        using type = std::tuple<__VA_ARGS__>;                               \
        static constexpr size_t size = std::tuple_size<type>();             \
        using func_type = std::function<void(audio::Buffer&, __VA_ARGS__)>; \
        static void apply(audio::Buffer&, __VA_ARGS__);                     \
    };

namespace audio {
DECLARE_EFFECT(Volume, float)
}; // namespace audio

#undef DECLARE_EFFECT
