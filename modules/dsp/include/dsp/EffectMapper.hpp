#pragma once

#include "IEffect.hpp"
#include <optional>
#include <vector>

namespace audio {
class EffectMapper {
  public:
    EffectMapper() noexcept;
    ~EffectMapper() noexcept;
    void add(size_t id, audio::EffectPtr effect) noexcept;
    std::vector<audio::EffectPtr> get(size_t id) const noexcept;
    void remove(size_t id, size_t effectId) noexcept;
    void removeAll(size_t id) noexcept;
    void clear() noexcept;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace audio
