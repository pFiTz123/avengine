#pragma once

#include "IEffect.hpp"
#include <functional>
#include <tuple>

namespace {
template <typename Function, typename Tuple, size_t... I>
auto call(Function f, audio::Buffer& b, Tuple t, std::index_sequence<I...>)
{
    return f(b, std::get<I>(t)...);
}

template <typename Function, typename Tuple>
auto call(Function f, audio::Buffer& b, Tuple t)
{
    static constexpr auto size = std::tuple_size<Tuple>::value;
    return call(f, b, t, std::make_index_sequence<size>{});
}
} // namespace

namespace audio {
template <typename T>
class Effect : public IEffect {
  public:
    void apply(audio::Buffer& buffer) const noexcept override
    {
        call(func, buffer, tuple);
    }

  protected:
    friend struct EffectFactory;
    Effect(const typename T::func_type& func, typename T::type tuple) noexcept
      : func(func)
      , tuple(tuple)
    {
    }
    typename T::func_type func;
    typename T::type tuple;
};
} // namespace audio
