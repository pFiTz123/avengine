#pragma once

#include "Effect.hpp"
#include "Effects.hpp"

namespace audio {
struct EffectFactory {
    template <typename T, typename... Args>
    static EffectPtr create(Args... args)
    {
        static_assert(sizeof...(args) == T::size, "Required arg count does not match!");
        static_assert(std::is_convertible_v<std::tuple<Args...>, typename T::type>, "Invalid types!");
        return std::shared_ptr<Effect<T>>(
            new Effect<T>(std::bind(T::apply, std::placeholders::_1, args...), std::make_tuple(args...)));
    }
};
} // namespace audio
