#include "dsp/Mix.hpp"
#include "system/Buffer.hpp"
#include "gtest/gtest.h"

using namespace audio;

TEST(MixTest, Mix)
{
    std::vector<Buffer> vecs{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
                             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
                             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
                             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
                             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
                             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
                             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
                             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}};
    auto res = vecs.front();
    for (const auto& v : vecs) {
        std::transform(res.begin(), res.end(), v.begin(), res.begin(), audio::alg1::mixSamples);
    }
    EXPECT_EQ(14, res.size());
}
