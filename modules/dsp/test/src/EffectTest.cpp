#include "gtest/gtest.h"

#include "system/Buffer.hpp"
#include "dsp/EffectFactory.hpp"

using namespace audio;

TEST(EffectTest, Create)
{
    EffectFactory factory;
    auto effect = factory.create<Volume>(.5f);
    audio::Buffer buffer(10);
    std::fill(std::begin(buffer), std::end(buffer), 10);
    effect->apply(buffer);
    for (const auto i : buffer) {
        EXPECT_EQ(5, i);
    }
}
