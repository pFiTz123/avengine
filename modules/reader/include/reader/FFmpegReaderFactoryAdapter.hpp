#pragma once

#include "IReaderFactory.hpp"

namespace audio {
class FFmpegReaderFactoryAdapter : public IReaderFactory {
  public:
    std::optional<ReaderPtr> create(sys::String file, SampleFormat fmt) noexcept override;
};
} // namespace audio
