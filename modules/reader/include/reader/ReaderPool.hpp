#pragma once

#include "IReader.hpp"
#include <optional>

namespace engine {

class ReaderPool {
  public:
    ReaderPool() noexcept;
    ~ReaderPool() noexcept;
    ReaderPool(const ReaderPool&) = delete;

    void add(audio::ReaderPtr reader) noexcept;
    void remove(size_t id) noexcept;
    std::vector<audio::ReaderPtr> get() const noexcept;
    std::optional<audio::ReaderPtr> get(const sys::String& file) const noexcept;
    std::optional<audio::ReaderPtr> get(size_t id) const noexcept;
    void clear() noexcept;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace engine
