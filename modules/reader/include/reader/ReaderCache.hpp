#pragma once

#include "system/Buffer.hpp"
#include "system/SampleFormat.hpp"
#include "system/Time.hpp"
#include <memory>
#include <optional>

namespace engine {

class ReaderCache {
  public:
    ReaderCache(audio::SampleFormat format) noexcept;
    ~ReaderCache() noexcept;

    void add(size_t id, audio::Buffer buffer, clk::Nanos pos, clk::Nanos dur) noexcept;
    std::optional<audio::Buffer> get(size_t id, clk::Nanos pos, clk::Nanos dur) const noexcept;
    void clear(size_t id) noexcept;
    void clearAll() noexcept;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace engine
