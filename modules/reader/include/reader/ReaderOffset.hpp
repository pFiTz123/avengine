#pragma once

#include "system/Time.hpp"
#include <memory>
#include <optional>

namespace engine {

class ReaderOffset {
  public:
    ReaderOffset() noexcept;
    ~ReaderOffset() noexcept;
    ReaderOffset(const ReaderOffset&) = delete;

    void set(size_t readerId, clk::Nanos offset) noexcept;
    std::optional<clk::Nanos> get(size_t readerId) const noexcept;
    void remove(size_t readerId) noexcept;
	void clear() noexcept;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace engine
