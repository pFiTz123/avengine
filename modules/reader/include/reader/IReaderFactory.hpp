#pragma once

#include "IReader.hpp"
#include "system/String.hpp"
#include <memory>
#include <optional>

namespace audio {
struct IReaderFactory {
    virtual ~IReaderFactory() noexcept = default;
    virtual std::optional<ReaderPtr> create(sys::String file, SampleFormat fmt) noexcept = 0;
};

using ReaderFactoryPtr = std::shared_ptr<IReaderFactory>;
} // namespace audio
