#pragma once

#include "IReader.hpp"

namespace audio {
class FFmpegReaderAdapter : public IReader {
  public:
    FFmpegReaderAdapter(SampleFormat format) noexcept;
    ~FFmpegReaderAdapter() noexcept override;
    bool init(sys::String string) noexcept override;
    const sys::String& file() const noexcept override;
    size_t id() const noexcept override;
    Buffer get() noexcept override;
    Buffer get(clk::Nanos duration) noexcept override;
    void seek(clk::Nanos clk) noexcept override;
    clk::Nanos position() const noexcept override;
    clk::Nanos duration() const noexcept override;
     SampleFormat format() const noexcept override;
    void setFormat(const SampleFormat&) noexcept override;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace audio
