#pragma once

#include "system/Buffer.hpp"
#include "system/SampleFormat.hpp"
#include "system/String.hpp"
#include "system/Time.hpp"
#include <memory>

namespace audio {
class IReader {
  public:
    virtual ~IReader() noexcept = default;
    virtual bool init(sys::String string) noexcept = 0;
    virtual size_t id() const noexcept = 0;
    virtual const sys::String& file() const noexcept = 0;
    virtual Buffer get() noexcept = 0;
    virtual Buffer get(clk::Nanos duration) noexcept = 0;
    virtual void seek(clk::Nanos clk) noexcept = 0;
    virtual clk::Nanos position() const noexcept = 0;
    virtual clk::Nanos duration() const noexcept = 0;
    virtual  SampleFormat format() const noexcept = 0;
    virtual void setFormat(const SampleFormat&) noexcept = 0;
};

using ReaderPtr = std::shared_ptr<IReader>;
} // namespace audio
