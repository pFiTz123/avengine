#include "reader/ReaderCache.hpp"
#include "system/Mutex.hpp"
#include "system/Utility.hpp"
#include <algorithm>
#include <cstring>

namespace {
struct CacheElement {
    CacheElement(audio::Buffer buffer, clk::Nanos pos, clk::Nanos dur)
      : buffer(buffer)
      , pos(pos)
      , dur(dur)
    {
    }
    audio::Buffer buffer;
    clk::Nanos pos;
    clk::Nanos dur;
};
using RingBuffer = std::vector<std::shared_ptr<CacheElement>>;
} // namespace

namespace engine {

struct ReaderCache::pImpl {
    pImpl(audio::SampleFormat format)
      : format(std::move(format))
    {
    }
    size_t toSamples(clk::Nanos dur)
    {
        return audio::toSamples(format.sampleRate, format.channels, dur);
    }
    Mutex mutex;
    audio::SampleFormat format;
    std::vector<size_t> ids;
    std::vector<RingBuffer> buffers;
};

ReaderCache::ReaderCache(audio::SampleFormat format) noexcept
  : p(std::make_unique<pImpl>(std::move(format)))
{
}

ReaderCache::~ReaderCache() noexcept
{
}

void ReaderCache::add(size_t id, audio::Buffer buffer, clk::Nanos pos, clk::Nanos dur) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found == std::end(p->ids)) {
        p->ids.emplace_back(id);
        p->buffers.emplace_back(RingBuffer{std::make_shared<CacheElement>(buffer, pos, dur)});
    }
    else {
        auto idx = std::distance(std::begin(p->ids), found);
        auto& ringBuffer = p->buffers.at(idx);
        auto ringPos = std::find_if(
            std::begin(ringBuffer), std::end(ringBuffer), [pos](const std::shared_ptr<CacheElement>& cacheElement) {
                return pos < cacheElement->pos;
            });
        ringBuffer.insert(ringPos, std::make_shared<CacheElement>(buffer, pos, dur));
    }
}

std::optional<audio::Buffer> ReaderCache::get(size_t id, clk::Nanos pos, clk::Nanos dur) const noexcept
{
    std::optional<audio::Buffer> buffer;
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        auto& ringBuffer = p->buffers.at(idx);
        auto ringPos = std::find_if(
            std::begin(ringBuffer), std::end(ringBuffer), [pos](const std::shared_ptr<CacheElement>& cacheElement) {
                return pos == cacheElement->pos;
            });
        if (ringPos != std::end(ringBuffer)) {
            auto samples = p->toSamples(dur);
            if (samples == (*ringPos)->buffer.size())
                buffer = (*ringPos)->buffer;
            else if (samples < (*ringPos)->buffer.size()) {
                audio::Buffer dst(samples);
                memcpy(&dst[0], &(*ringPos)->buffer[0], samples * sizeof(int16_t));
                buffer = dst;
            }
            else {
                audio::Buffer dst(samples);
                size_t idx = 0;
                clk::Nanos pos = (*ringPos)->pos;
                do {
                    auto& cacheElement = *ringPos;
                    memcpy(&dst[idx], &cacheElement->buffer[0], cacheElement->buffer.size() * sizeof(int16_t));
                    samples -= cacheElement->buffer.size();
                    idx += cacheElement->buffer.size();
                    pos += cacheElement->dur;
                    ringPos = std::next(ringPos);
                } while (ringPos != std::end(ringBuffer) && samples > 0 && (*ringPos)->pos == pos);
                if (samples > 0 && samples != dst.size()) {
                    dst.resize(dst.size() - samples);
                }
                buffer = dst;
            }
        }
    }
    return buffer;
}

void ReaderCache::clear(size_t id) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        p->ids.erase(found);
        p->buffers.erase(std::begin(p->buffers) + idx);
    }
}

void ReaderCache::clearAll() noexcept
{
    ScopedLock lck(p->mutex);
    p->ids.clear();
    p->buffers.clear();
}

} // namespace engine
