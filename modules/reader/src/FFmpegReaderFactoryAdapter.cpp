#include "reader/FFmpegReaderFactoryAdapter.hpp"
#include "reader/FFmpegReaderAdapter.hpp"

namespace audio {
std::optional<ReaderPtr> FFmpegReaderFactoryAdapter::create(sys::String file, SampleFormat fmt) noexcept
{
    auto reader = std::make_shared<FFmpegReaderAdapter>(fmt);
    if (reader->init(file)) {
        return reader;
    }
    return std::nullopt;
}
} // namespace audio
