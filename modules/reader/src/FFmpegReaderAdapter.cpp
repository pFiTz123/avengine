#include "reader/FFmpegReaderAdapter.hpp"
#include "ffmpeg/audio/Reader.hpp"
#include <memory>

namespace {
ffmpeg::audio::SampleFormat convertFormat(audio::SampleFormat format)
{
    return ffmpeg::audio::SampleFormat(
        format.sampleRate, format.bitsPerSample, format.channels, ffmpeg::audio::SampleFormat::Format::SAMPLE_FMT_S16);
}
audio::SampleFormat convertFormat(ffmpeg::audio::SampleFormat format)
{
    return audio::SampleFormat(format.sampleRate, format.bitsPerSample, format.channels);
}
} // namespace
namespace audio {
struct FFmpegReaderAdapter::pImpl {
    pImpl(SampleFormat format)
      : format(format)
      , reader(convertFormat(format))
    {
    }
    SampleFormat format;
    ffmpeg::audio::FFmpegReader reader;
};

FFmpegReaderAdapter::FFmpegReaderAdapter(SampleFormat format) noexcept
  : p(std::make_unique<pImpl>(format))
{
}

FFmpegReaderAdapter::~FFmpegReaderAdapter() noexcept
{
}

bool FFmpegReaderAdapter::init(sys::String string) noexcept
{
    return p->reader.init(string);
}
const sys::String& FFmpegReaderAdapter::file() const noexcept
{
    return p->reader.file();
}
size_t FFmpegReaderAdapter::id() const noexcept
{
    return p->reader.id();
}
Buffer FFmpegReaderAdapter::get() noexcept
{
    return p->reader.get();
}
Buffer FFmpegReaderAdapter::get(clk::Nanos duration) noexcept
{
    return p->reader.get(duration);
}
void FFmpegReaderAdapter::seek(clk::Nanos clk) noexcept
{
    return p->reader.seek(clk);
}
clk::Nanos FFmpegReaderAdapter::position() const noexcept
{
    return p->reader.position();
}
clk::Nanos FFmpegReaderAdapter::duration() const noexcept
{
    return p->reader.duration();
}
 SampleFormat FFmpegReaderAdapter::format() const noexcept
{
    return convertFormat(p->reader.format());
}

void FFmpegReaderAdapter::setFormat(const SampleFormat& fmt) noexcept
{
    // TODO impl @ffmpeg-reader
}
} // namespace audio
