#include "reader/ReaderPool.hpp"
#include "system/Mutex.hpp"
#include <algorithm>
#include <vector>

namespace engine {

struct ReaderPool::pImpl {
    using Ids = std::vector<size_t>;
    using Readers = std::vector<audio::ReaderPtr>;
    Mutex mutex;
    Readers readers;
    Ids ids;
};

ReaderPool::ReaderPool() noexcept
  : p(std::make_unique<pImpl>())
{
}

ReaderPool::~ReaderPool() noexcept
{
}

void ReaderPool::add(audio::ReaderPtr reader) noexcept
{
    ScopedLock lck(p->mutex);
    p->ids.emplace_back(reader->id());
    p->readers.emplace_back(reader);
}

void ReaderPool::remove(size_t id) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found != std::end(p->ids)) {
        p->readers.erase(std::begin(p->readers) + std::distance(std::begin(p->ids), found));
        p->ids.erase(found);
    }
}

std::vector<audio::ReaderPtr> ReaderPool::get() const noexcept
{
    ScopedLock lck(p->mutex);
    return p->readers;
}

std::optional<audio::ReaderPtr> ReaderPool::get(size_t id) const noexcept
{
    ScopedLock lck(p->mutex);
    std::optional<audio::ReaderPtr> reader;
    auto found = std::find(std::begin(p->ids), std::end(p->ids), id);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        reader = p->readers.at(idx);
    }
    return reader;
}

std::optional<audio::ReaderPtr> ReaderPool::get(const sys::String& file) const noexcept
{
    ScopedLock lck(p->mutex);
    std::optional<audio::ReaderPtr> reader;
    auto found
        = std::find_if(std::begin(p->readers), std::end(p->readers), [& file = file](const audio::ReaderPtr& reader) {
              return file.compare(reader->file()) == 0;
          });
    if (found != std::end(p->readers)) {
        reader = *found;
    }
    return reader;
}

void ReaderPool::clear() noexcept
{
    ScopedLock lck(p->mutex);
    p->ids.clear();
    p->readers.clear();
}
} // namespace engine
