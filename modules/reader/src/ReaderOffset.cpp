#include "reader/ReaderOffset.hpp"
#include "system/Mutex.hpp"
#include <algorithm>
#include <vector>

namespace engine {

struct ReaderOffset::pImpl {
    Mutex mutex;
    std::vector<size_t> ids;
    std::vector<clk::Nanos> offsets;
};

ReaderOffset::ReaderOffset() noexcept
  : p(std::make_unique<pImpl>())
{
}

ReaderOffset::~ReaderOffset() noexcept
{
}

void ReaderOffset::set(size_t readerId, clk::Nanos offset) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), readerId);
    if (found == std::end(p->ids)) {
        p->ids.emplace_back(readerId);
        p->offsets.emplace_back(offset);
    }
    else {
        auto idx = std::distance(std::begin(p->ids), found);
        p->offsets.at(idx) = offset;
    }
}

std::optional<clk::Nanos> ReaderOffset::get(size_t readerId) const noexcept
{
    std::optional<clk::Nanos> offset;
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), readerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        offset = p->offsets.at(idx);
    }
    return offset;
}

void ReaderOffset::remove(size_t readerId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), readerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        p->ids.erase(found);
        p->offsets.erase(std::begin(p->offsets) + idx);
    }
}

void ReaderOffset::clear() noexcept
{
    ScopedLock lck(p->mutex);
    p->ids.clear();
    p->offsets.clear();
}
} // namespace engine
