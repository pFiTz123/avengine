#include "gtest/gtest.h"

#include "reader/ReaderCache.hpp"
#include "system/Utility.hpp"

using namespace engine;

namespace {
constexpr clk::Seconds DEFAULT_REQUEST_DURATION{1};
constexpr audio::SampleFormat DEFAULT_SAMPLE_FORMAT = audio::SampleFormat(44100, 16, 2);
size_t toSamples(clk::Nanos dur = DEFAULT_REQUEST_DURATION, audio::SampleFormat format = DEFAULT_SAMPLE_FORMAT)
{
    return audio::toSamples(format.sampleRate, format.channels, dur);
}

audio::Buffer createBuffer(clk::Nanos dur = DEFAULT_REQUEST_DURATION,
                           audio::SampleFormat format = DEFAULT_SAMPLE_FORMAT)
{
    return audio::Buffer(toSamples(dur, format));
}
} // namespace

TEST(ReaderCacheTest, AddGet)
{
    ReaderCache cache(DEFAULT_SAMPLE_FORMAT);
    cache.add(0, createBuffer(), clk::Nanos{0}, clk::Seconds{1});
    auto buffer0 = cache.get(0, clk::Nanos{0}, clk::Seconds{1});
    ASSERT_TRUE(buffer0.has_value());
    EXPECT_EQ(toSamples(), buffer0.value().size());
}

TEST(ReaderCacheTest, AddGet_Multiple)
{
    ReaderCache cache(DEFAULT_SAMPLE_FORMAT);
    cache.add(0, createBuffer(), clk::Nanos{0}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{10}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{11}, clk::Seconds{1});
    cache.add(5, createBuffer(clk::Millis{100}), clk::Nanos{22}, clk::Millis{100});

    auto buffer1 = cache.get(1, clk::Seconds{10}, clk::Seconds{2});
    ASSERT_TRUE(buffer1.has_value());
    EXPECT_EQ(toSamples() * 2, buffer1.value().size());
}

TEST(ReaderCacheTest, AddGet_Gap)
{
    ReaderCache cache(DEFAULT_SAMPLE_FORMAT);
    cache.add(0, createBuffer(), clk::Nanos{0}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{10}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{12}, clk::Seconds{1});
    cache.add(5, createBuffer(clk::Millis{100}), clk::Nanos{22}, clk::Millis{100});

    auto buffer1 = cache.get(1, clk::Seconds{10}, clk::Seconds{2});
    ASSERT_TRUE(buffer1.has_value());
    EXPECT_EQ(toSamples(), buffer1.value().size());
}

TEST(ReaderCacheTest, AddGet_Part)
{
    ReaderCache cache(DEFAULT_SAMPLE_FORMAT);
    cache.add(0, createBuffer(), clk::Nanos{0}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{10}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{11}, clk::Seconds{1});
    cache.add(5, createBuffer(clk::Millis{100}), clk::Nanos{22}, clk::Millis{100});

    auto buffer1 = cache.get(1, clk::Seconds{10}, clk::Millis{100});
    ASSERT_TRUE(buffer1.has_value());
    EXPECT_EQ(toSamples(clk::Millis{100}), buffer1.value().size());
}

TEST(ReaderCacheTest, AddGet_Exceed)
{
    ReaderCache cache(DEFAULT_SAMPLE_FORMAT);
    cache.add(0, createBuffer(), clk::Nanos{0}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{10}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{11}, clk::Seconds{1});
    cache.add(5, createBuffer(clk::Millis{100}), clk::Nanos{22}, clk::Millis{100});

    auto buffer1 = cache.get(1, clk::Seconds{10}, clk::Seconds{3}); // just 2 seconds exist
    ASSERT_TRUE(buffer1.has_value());
    EXPECT_EQ(toSamples() * 2, buffer1.value().size());
}

TEST(ReaderCacheTest, AddGet_Fail)
{
    ReaderCache cache(DEFAULT_SAMPLE_FORMAT);
    cache.add(0, createBuffer(), clk::Nanos{0}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{10}, clk::Seconds{1});
    cache.add(1, createBuffer(), clk::Seconds{12}, clk::Seconds{1});
    cache.add(5, createBuffer(clk::Millis{100}), clk::Nanos{22}, clk::Millis{100});

    auto buffer = cache.get(2, clk::Seconds{10}, clk::Millis{100}); // id does not exist
    ASSERT_FALSE(buffer.has_value());

    buffer = cache.get(1, clk::Seconds{9}, clk::Millis{100}); // pos does not exist
    ASSERT_FALSE(buffer.has_value());
}
