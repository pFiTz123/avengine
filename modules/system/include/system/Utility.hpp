#pragma once

#include "system/Time.hpp"
#include <cmath>
#include <numeric>
#include <vector>

namespace audio {

constexpr uint32_t SecondsNanosFactor = 1000000000;

inline int64_t toSamplePosition(clk::Nanos clk, int64_t samplerate)
{
    return static_cast<int64_t>(clk::to_seconds(clk) * static_cast<double>(samplerate));
}

inline clk::Nanos toTime(int64_t samplePosition, int64_t samplerate)
{
    return samplerate == 0 ? clk::Nanos{0} : clk::Nanos{samplePosition / samplerate};
}

inline double toSeconds(size_t samples, uint32_t sampleRate, uint8_t channels)
{
    return static_cast<double>(samples) / static_cast<double>(sampleRate) / static_cast<double>(channels);
}

inline size_t toSamples(uint32_t sampleRate, uint8_t channels, clk::Nanos nanos)
{
    return static_cast<size_t>(std::round(sampleRate * channels * clk::to_seconds(nanos)));
}

template <typename T>
std::vector<T> flatten(const std::vector<std::vector<T>>& v)
{
    size_t size = std::accumulate(
        std::begin(v), std::end(v), 0u, [](size_t a, const auto& b) -> size_t { return a + b.size(); });
    std::vector<T> result;
    result.reserve(size);
    for (const auto& sub : v)
        result.insert(std::end(result), std::begin(sub), std::end(sub));
    return result;
}
} // namespace audio
