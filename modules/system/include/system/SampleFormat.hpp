#pragma once

#include <cstdint>

namespace audio {

using Samplerate = uint32_t;
using BitsPerSample = uint8_t;
using Channels = uint8_t;

namespace {
constexpr Samplerate SAMPLE_RATE{44100};
constexpr BitsPerSample BITSPERSAMPLE{8};
constexpr Channels CHANNELS{2};
} // namespace

struct SampleFormat {
    constexpr SampleFormat()
      : sampleRate(SAMPLE_RATE)
      , bitsPerSample(BITSPERSAMPLE)
      , channels(CHANNELS)
    {
    }
    constexpr SampleFormat(Samplerate rate, BitsPerSample bps, Channels chan)
      : sampleRate(rate)
      , bitsPerSample(bps)
      , channels(chan)
    {
    }
    Samplerate sampleRate;
    BitsPerSample bitsPerSample;
    Channels channels;
};
} // namespace audio
