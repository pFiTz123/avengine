#pragma once

#include "system/String.hpp"
#include "system/Time.hpp"
#include <chrono>
#include <memory>

class Logger {
  public:
    enum class Level { All, Off, Debug, Info, Warning, Error, Fatal };

    Logger();

    static std::shared_ptr<Logger> instance();

    template <typename... Args>
    void log(Level, const char*, const Args&...);
    void log(Level, const sys::String&);

    Level level() const;
    void setLevel(Level level);
    template <typename F>
    void measure(const sys::String& tag, F&& f)
    {
        auto time = clk::measure<clk::Millis>(std::forward<F>(f));
        log(Level::Debug, sys::format("[%s] Measure: %lldms", tag, time.count()));
    }

  private:
    struct pImpl;
    pImpl* p;
};

template <typename... Args>
inline void Logger::log(Level level, const char* fmt, const Args&... args)
{
    log(level, sys::format(fmt, args...));
}

#ifdef ENABLE_LOGGING
#    define LOGD(message) Logger::instance()->log(Logger::Level::Debug, message)
#    define LOGI(message) Logger::instance()->log(Logger::Level::Info, message)
#    define LOGW(message) Logger::instance()->log(Logger::Level::Warning, message)
#    define LOGE(message) Logger::instance()->log(Logger::Level::Error, message)
#    define LOGF(message) Logger::instance()->log(Logger::Level::Fatal, message)
#    define LOGDV(message, ...) Logger::instance()->log(Logger::Level::Debug, message, __VA_ARGS__)
#    define LOGIV(message, ...) Logger::instance()->log(Logger::Level::Info, message, __VA_ARGS__)
#    define LOGWV(message, ...) Logger::instance()->log(Logger::Level::Warning, message, __VA_ARGS__)
#    define LOGEV(message, ...) Logger::instance()->log(Logger::Level::Error, message, __VA_ARGS__)
#    define LOGFV(message, ...) Logger::instance()->log(Logger::Level::Fatal, message, __VA_ARGS__)
#else
#    define LOGD(message)
#    define LOGI(message)
#    define LOGW(message)
#    define LOGE(message)
#    define LOGF(message)
#    define LOGDV(message, ...)
#    define LOGIV(message, ...)
#    define LOGWV(message, ...)
#    define LOGEV(message, ...)
#    define LOGFV(message, ...)
#endif
