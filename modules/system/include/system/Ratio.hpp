#pragma once

#include "boost/rational.hpp"

namespace sys {

using Ratio = boost::rational<int32_t>;
using Framerate = Ratio;

} // namespace sys
