#pragma once

#include <chrono>

namespace clk {

using Nanos = std::chrono::nanoseconds;
using Micros = std::chrono::microseconds;
using Millis = std::chrono::milliseconds;
using Seconds = std::chrono::seconds;
using Timestamp = std::chrono::steady_clock::time_point;
constexpr auto now = std::chrono::steady_clock::now;
template <typename Time>
constexpr double to_seconds(Time time)
{
    return std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1, 1>>>(time).count();
}
template <typename Time>
constexpr Time from_seconds(double seconds)
{
    return std::chrono::duration_cast<Time>(std::chrono::duration<double, std::ratio<1, 1>>(seconds));
}
template <typename Time, typename F>
constexpr Time measure(F&& f)
{
    auto s = now();
    f();
    return std::chrono::duration_cast<Time>(now() - s);
}
} // namespace clk
