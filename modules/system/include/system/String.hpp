#pragma once

#include <memory>
#include <string>

namespace sys {

using String = std::string;
template <typename... Args>
inline String format(const char* fmt, const Args&... args)
{
    size_t size = std::snprintf(nullptr, 0, fmt, args...) + 1; // Extra space for '\0'
    std::unique_ptr<char[]> buf(new char[size]);
    std::snprintf(buf.get(), size, fmt, args...);
    return String(buf.get(), buf.get() + size - 1);
}
} // namespace sys
