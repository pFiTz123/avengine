#pragma once

#include <condition_variable>
#include <mutex>
#include <shared_mutex>

using Mutex = std::shared_mutex;
using ReadLock = std::shared_lock<Mutex>;
using ScopedLock = std::scoped_lock<Mutex>;
using UniqueLock = std::unique_lock<Mutex>;

using Condition = std::condition_variable;
using ConditionAny = std::condition_variable_any;
