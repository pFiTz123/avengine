#pragma once

#include <cstdint>
#include <vector>

namespace audio {
using Buffer = std::vector<int16_t>;
} // namespace audio
