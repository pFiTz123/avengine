cmake_minimum_required(VERSION 3.0)

enable_testing()
file(GLOB_RECURSE testsources src/*.*)
add_executable(${PROJECT_NAME}_test ${testsources})
target_link_libraries(${PROJECT_NAME}_test PRIVATE CONAN_PKG::gtest ${PROJECT_NAME})
add_test(NAME Test_${PROJECT_NAME} COMMAND $<TARGET_FILE:${PROJECT_NAME}_test>)