#pragma once

#include <android/log.h>
#include <log4cplus/appender.h>
#include <log4cplus/spi/loggingevent.h>

class AndroidAppender : public log4cplus::Appender {
  public:
    void close() override
    {
    }

  protected:
    void append(const log4cplus::spi::InternalLoggingEvent& event) override
    {
        auto level = event.getLogLevel();
        if (level == log4cplus::OFF_LOG_LEVEL)
            return;

        android_LogPriority logProp;
        switch (level) {
            case log4cplus::ALL_LOG_LEVEL:
                logProp = ANDROID_LOG_VERBOSE;
                break;
            case log4cplus::DEBUG_LOG_LEVEL:
                logProp = ANDROID_LOG_DEBUG;
                break;
            case log4cplus::INFO_LOG_LEVEL:
                logProp = ANDROID_LOG_INFO;
                break;
            case log4cplus::WARN_LOG_LEVEL:
                logProp = ANDROID_LOG_WARN;
                break;
            case log4cplus::ERROR_LOG_LEVEL:
                logProp = ANDROID_LOG_ERROR;
                break;
            case log4cplus::FATAL_LOG_LEVEL:
                logProp = ANDROID_LOG_FATAL;
                break;
        }
        __android_log_print(logProp, event.getLoggerName().c_str(), "%s", event.getMessage().c_str());
    }
};
