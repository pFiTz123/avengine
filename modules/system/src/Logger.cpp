#include "system/Logger.hpp"

#include <log4cplus/configurator.h>
#include <log4cplus/initializer.h>
#include <log4cplus/logger.h>

#ifdef ANDROID
#    include "AndroidAppender.hpp"
#endif

namespace {
log4cplus::LogLevel toLogLevel(Logger::Level lvl)
{
    switch (lvl) {
        case Logger::Level::All:
            return log4cplus::ALL_LOG_LEVEL;
        case Logger::Level::Off:
            return log4cplus::OFF_LOG_LEVEL;
        case Logger::Level::Debug:
            return log4cplus::DEBUG_LOG_LEVEL;
        case Logger::Level::Info:
            return log4cplus::INFO_LOG_LEVEL;
        case Logger::Level::Warning:
            return log4cplus::WARN_LOG_LEVEL;
        case Logger::Level::Error:
            return log4cplus::ERROR_LOG_LEVEL;
        case Logger::Level::Fatal:
            return log4cplus::FATAL_LOG_LEVEL;
        default:
            return log4cplus::DEBUG_LOG_LEVEL;
    }
}
} // namespace
struct Logger::pImpl {
    pImpl()
    {
        log4cplus::BasicConfigurator::doConfigure();
        setLevel(level);
#ifdef ANDROID
        logger.addAppender(log4cplus::SharedAppenderPtr(new AndroidAppender()));
#endif
    }
    log4cplus::Logger logger = log4cplus::Logger::getInstance(LOG4CPLUS_STRING_TO_TSTRING("Engine"));
    Logger::Level level = Level::All;

    void setLevel(Level level)
    {
        logger.setLogLevel(toLogLevel(level));
        this->level = level;
    }
};

Logger::Logger()
  : p(new pImpl())
{
}

std::shared_ptr<Logger> Logger::instance()
{
    static std::shared_ptr<Logger> instance = std::make_shared<Logger>();
    return instance;
}

void Logger::log(Level lvl, const sys::String& str)
{
    p->logger.log(toLogLevel(lvl), LOG4CPLUS_STRING_TO_TSTRING(str));
}
Logger::Level Logger::level() const
{
    return p->level;
}
void Logger::setLevel(Level level)
{
    p->setLevel(level);
}
