#include "project/Project.hpp"
#include "project/Arranger.hpp"
#include "system/Mutex.hpp"

namespace pro {

struct Project::pImpl {

    Mutex mutex;
    std::vector<size_t> ids;
    std::vector<ArrangerPtr> arrangers;
};

Project::Project()
  : p(std::make_unique<pImpl>())
{
}

Project::~Project()
{
}

// Arranger
ArrangerPtr Project::create() noexcept
{
    ScopedLock lck(p->mutex);
    auto arranger = std::make_shared<Arranger>();
    p->ids.emplace_back(arranger->id());
    p->arrangers.emplace_back(arranger);
    return arranger;
}

std::optional<ArrangerPtr> Project::getArranger(size_t arrangerId) noexcept
{
    std::optional<ArrangerPtr> ret;
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        ret = p->arrangers.at(idx);
    }
    return ret;
}

Arrangers Project::getArrangers() const noexcept
{
    ScopedLock lck(p->mutex);
    return p->arrangers;
}

void Project::removeArranger(size_t arrangerId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        p->ids.erase(found);
        p->arrangers.erase(std::begin(p->arrangers) + idx);
    }
}

void Project::clearArranger(size_t arrangerId) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        p->arrangers.at(idx)->clear();
    }
}

void Project::clearArrangers() noexcept
{
    ScopedLock lck(p->mutex);
    for (const auto& arranger : p->arrangers) {
        arranger->clear();
    }
}

// Track
std::optional<Tracks> Project::getTracks(size_t arrangerId) const noexcept
{
    std::optional<Tracks> tracks;
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        tracks = p->arrangers.at(idx)->tracks();
    }
    return tracks;
}

void Project::removeTrack(size_t arrangerId, size_t trackIndex) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        p->arrangers.at(idx)->removeTrack(trackIndex);
    }
}

void Project::clearTrack(size_t arrangerId, size_t trackIndex) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        auto tracks = p->arrangers.at(idx)->tracks();
        if (!tracks.empty() && trackIndex < tracks.size()) {
            tracks.at(trackIndex)->clear();
        }
    }
}

// Entry
void Project::addEntry(size_t arrangerId, size_t trackIndex, Entry entry) noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        p->arrangers.at(idx)->addEntry(trackIndex, entry);
    }
}

void Project::removeEntry(size_t entryId) noexcept
{
    ScopedLock lck(p->mutex);
    for (const auto& arranger : p->arrangers) {
        arranger->removeEntry(entryId);
    }
}

TimeInfos Project::at(clk::Nanos position) const noexcept
{
    TimeInfos infos;
    ScopedLock lck(p->mutex);
    for (const auto& arranger : p->arrangers) {
        auto tracks = arranger->tracks();
        for (const auto& track : tracks) {
            infos.emplace_back(TimeInfo{arranger, track, track->at(position)});
        }
    }
    return infos;
}

TimeInfos Project::within(clk::Nanos start, clk::Nanos end) const noexcept
{
    TimeInfos infos;
    ScopedLock lck(p->mutex);
    for (const auto& arranger : p->arrangers) {
        auto tracks = arranger->tracks();
        for (const auto& track : tracks) {
            infos.emplace_back(TimeInfo{arranger, track, track->within(start, end)});
        }
    }
    return infos;
}

std::optional<TimeInfos> Project::at(size_t arrangerId, clk::Nanos position) const noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        auto& arranger = p->arrangers.at(idx);
        TimeInfos infos;
        for (const auto& track : arranger->tracks()) {
            infos.emplace_back(TimeInfo{arranger, track, track->at(position)});
        }
        return infos;
    }
    return std::nullopt;
}

std::optional<TimeInfos> Project::within(size_t arrangerId, clk::Nanos start, clk::Nanos end) const noexcept
{
    ScopedLock lck(p->mutex);
    auto found = std::find(std::begin(p->ids), std::end(p->ids), arrangerId);
    if (found != std::end(p->ids)) {
        auto idx = std::distance(std::begin(p->ids), found);
        auto& arranger = p->arrangers.at(idx);
        TimeInfos infos;
        for (const auto& track : arranger->tracks()) {
            infos.emplace_back(TimeInfo{arranger, track, track->within(start, end)});
        }
        return infos;
    }
    return std::nullopt;
}

} // namespace pro
