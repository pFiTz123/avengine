#include "project/Arranger.hpp"
#include "project/Track.hpp"
#include <algorithm>
#include <atomic>
#include <cmath>

namespace {
std::atomic<size_t> nextId{0};
}

namespace pro {

Arranger::Arranger() noexcept
  : id_(++nextId)
{
}

size_t Arranger::id() const noexcept
{
    return id_;
}

clk::Nanos Arranger::start() const noexcept
{
    using limit = std::numeric_limits<int64_t>;
    clk::Nanos min{limit::max()};
    for (const auto& t : tracks_) {
        min = std::min(t->start(), min);
    }
    return min == clk::Nanos{limit::max()} ? clk::Nanos{0} : min;
}

clk::Nanos Arranger::end() const noexcept
{
    clk::Nanos max{0};
    for (const auto& t : tracks_) {
        max = std::max(t->end(), max);
    }
    return max;
}

clk::Nanos Arranger::duration() const noexcept
{
    return end() - start();
}

void Arranger::addTrack() noexcept
{
    tracks_.emplace_back(std::make_unique<Track>(tracks_.size()));
}

void Arranger::removeTrack(size_t trackIndex) noexcept
{
    if (trackIndex < tracks_.size()) {
        tracks_.erase(std::begin(tracks_) + trackIndex);
        for (size_t i = trackIndex; i < tracks_.size(); ++i) {
            tracks_.at(i)->setIndex(i);
        }
    }
}

void Arranger::swap(size_t trackA, size_t trackB) noexcept
{
    if (trackA < tracks_.size() && trackB < tracks_.size()) {
        std::iter_swap(std::begin(tracks_) + trackA, std::begin(tracks_) + trackB);
    }
}

void Arranger::addEntry(size_t trackIndex, Entry entry) noexcept
{
    if (trackIndex >= tracks_.size()) {
        for (size_t i = tracks_.size(); i <= trackIndex; ++i) {
            addTrack();
        }
    }
    tracks_.at(trackIndex)->add(entry);
}

void Arranger::removeEntry(size_t entryId) noexcept
{
    for (auto& track : tracks_) {
        track->remove(entryId);
    }
}
Tracks Arranger::tracks() const noexcept
{
    return tracks_;
}

size_t Arranger::size() const noexcept
{
    return tracks_.size();
}

void Arranger::clear() noexcept
{
    tracks_.clear();
}

} // namespace pro
