#include "project/Entry.hpp"
#include <atomic>

namespace {
std::atomic<size_t> nextId{0};
}

namespace pro {
Entry::Entry(int64_t start, int64_t end) noexcept
  : id_(++nextId)
  , start_(start)
  , end_(end)
{
}

Entry::Entry(clk::Nanos start, clk::Nanos end) noexcept
  : id_(++nextId)
  , start_(start)
  , end_(end)
{
}

bool Entry::operator==(const Entry& e) const noexcept
{
    return e.id_ == id_ && e.start_ == start_ && e.end_ == end_;
}

size_t Entry::id() const noexcept
{
    return id_;
}

clk::Nanos Entry::start() const noexcept
{
    return start_;
}

clk::Nanos Entry::end() const noexcept
{
    return end_;
}

} // namespace pro
