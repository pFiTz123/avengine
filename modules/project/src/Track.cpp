#include "project/Track.hpp"

#include <algorithm>
#include <atomic>

namespace {
bool isAt(clk::Nanos pos, clk::Nanos min, clk::Nanos max)
{
    return pos >= min && pos < max;
}
bool isWithin(clk::Nanos pos, clk::Nanos min, clk::Nanos max)
{
    return pos >= min && max > pos;
}

template <typename It>
It find_entry(It first, It last, size_t id) noexcept
{
    return std::find_if(first, last, [id = id](const auto& entry) { return id == entry.id(); });
}
} // namespace

namespace pro {

Track::Track(size_t index) noexcept
  : index_(index)
{
}

size_t Track::index() const noexcept
{
    return index_;
}

void Track::setIndex(size_t index) noexcept
{
    index_ = index;
}

void Track::add(Entry entry) noexcept
{
    entries.emplace_back(entry);
}

Entries Track::at(clk::Nanos clk) const noexcept
{
    Entries timeEntries;
    std::copy_if(std::begin(entries),
                 std::end(entries),
                 std::back_inserter(timeEntries),
                 [clk = clk](const Entry& entry) { return isAt(clk, entry.start(), entry.end()); });
    return timeEntries;
}

Entries Track::within(clk::Nanos start, clk::Nanos end) const noexcept
{
    Entries timeEntries;
    std::copy_if(
        std::begin(entries), std::end(entries), std::back_inserter(timeEntries), [start, end](const Entry& entry) {
            return (start >= entry.start() && start < entry.end()) || (end >= entry.start() && entry.end() > start);
        });
    return timeEntries;
}

std::optional<Entry> Track::get(size_t id) const noexcept
{
    std::optional<Entry> ret;
    auto found = find_entry(std::begin(entries), std::end(entries), id);
    if (found != std::end(entries)) {
        ret = *found;
    }
    return ret;
}

Entries Track::getEntries() const noexcept
{
    return entries;
}

size_t Track::size() const noexcept
{
    return entries.size();
}

void Track::remove(Entry entry) noexcept
{
    entries.erase(std::remove(std::begin(entries), std::end(entries), entry), std::end(entries));
}

void Track::update(Entry entry) noexcept
{
    auto e = std::find_if(
        std::begin(entries), std::end(entries), [id = entry.id()](const auto& entry) { return entry.id() == id; });
    if (e != std::end(entries)) {
        (*e) = entry;
    }
}

clk::Nanos Track::start() const noexcept
{
    clk::Nanos min{std::numeric_limits<int64_t>::max()};
    for (const auto& e : entries) {
        min = std::min(e.start(), min);
    }
    return min;
}

clk::Nanos Track::end() const noexcept
{
    clk::Nanos max{0};
    for (const auto& e : entries) {
        max = std::max(e.end(), max);
    }
    return max;
}

clk::Nanos Track::duration() const noexcept
{
    if (entries.empty()) {
        return clk::Nanos(0);
    }

    clk::Nanos min{std::numeric_limits<int64_t>::max()};
    clk::Nanos max{0};
    for (const auto& e : entries) {
        min = std::min(e.start(), min);
        max = std::max(e.end(), max);
    }
    if (min.count() == std::numeric_limits<int64_t>::max())
        min = clk::Nanos{0};
    return max - min;
}

void Track::remove(size_t entryId) noexcept
{
    auto found = find_entry(std::begin(entries), std::end(entries), entryId);
    if (found != std::end(entries)) {
        entries.erase(found);
    }
}

void Track::clear() noexcept
{
    entries.clear();
}
} // namespace pro
