#pragma once

#include "Entry.hpp"
#include "system/Time.hpp"
#include <memory>
#include <optional>
#include <vector>

namespace pro {
using Entries = std::vector<Entry>;

struct ITrack {
  public:
    virtual ~ITrack() = default;
    virtual size_t index() const noexcept = 0;
	virtual void setIndex(size_t index) noexcept = 0;
    virtual void add(Entry entry) noexcept = 0;
    virtual Entries at(clk::Nanos clk) const noexcept = 0;
    virtual Entries within(clk::Nanos start, clk::Nanos end) const noexcept = 0;
    virtual std::optional<Entry> get(size_t id) const noexcept = 0;
    virtual Entries getEntries() const noexcept = 0;
    virtual void remove(Entry entry) noexcept = 0;
    virtual void remove(size_t entryId) noexcept = 0;
    virtual void update(Entry entry) noexcept = 0;
    virtual size_t size() const noexcept = 0;
    virtual clk::Nanos start() const noexcept = 0;
    virtual clk::Nanos end() const noexcept = 0;
    virtual clk::Nanos duration() const noexcept = 0;
    virtual void clear() noexcept = 0;
};

using TrackPtr = std::shared_ptr<ITrack>;
} // namespace pro
