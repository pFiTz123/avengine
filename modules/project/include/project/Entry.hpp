#pragma once

#include "system/Time.hpp"

namespace pro {

class Entry {
  public:
    Entry(const Entry &e) noexcept = default;
    Entry(Entry &&e) noexcept = default;
    Entry &operator=(const Entry &e) noexcept = default;
    Entry &operator=(Entry &&e) noexcept = default;
    bool operator==(const Entry &e) const noexcept;
    size_t id() const noexcept;
    clk::Nanos start() const noexcept;
    clk::Nanos end() const noexcept;

  protected:
    friend struct EntryFactory;
    Entry(int64_t start, int64_t end) noexcept;
    Entry(clk::Nanos start, clk::Nanos end) noexcept;

  protected:
    size_t id_;
    clk::Nanos start_;
    clk::Nanos end_;
};
} // namespace pro
