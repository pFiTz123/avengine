#pragma once

#include "Entry.hpp"
#include <optional>

namespace pro {

struct EntryFactory {
    static std::optional<Entry> create(int64_t start, int64_t end)
    {
        return create(clk::Nanos{start}, clk::Nanos{end});
    }
    static std::optional<Entry> create(clk::Nanos start, clk::Nanos end)
    {
        if (start > end)
            return std::nullopt;
        return Entry(start, end);
    }
};
} // namespace pro
