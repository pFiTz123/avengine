#pragma once

#include "ITrack.hpp"
#include "system/Time.hpp"
#include <memory>
#include <vector>

namespace pro {
using Tracks = std::vector<TrackPtr>;
struct IArranger {
    virtual ~IArranger() = default;
    virtual size_t id() const noexcept = 0;
    virtual clk::Nanos duration() const noexcept = 0;
    virtual clk::Nanos start() const noexcept = 0;
    virtual clk::Nanos end() const noexcept = 0;
    virtual void addEntry(size_t trackIndex, Entry entry) noexcept = 0;
    virtual void addTrack() noexcept = 0;
    virtual void removeTrack(size_t trackIndex) noexcept = 0;
    virtual void removeEntry(size_t entryId) noexcept = 0;
    virtual Tracks tracks() const noexcept = 0;
    virtual void clear() noexcept = 0;
    virtual void swap(size_t trackA, size_t trackB) noexcept = 0;
    virtual size_t size() const noexcept = 0;
};

using ArrangerPtr = std::shared_ptr<IArranger>;
} // namespace pro
