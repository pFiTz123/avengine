#pragma once

#include "IArranger.hpp"
#include <memory>
#include <optional>
#include <tuple>

namespace pro {

using TimeInfo = std::tuple<ArrangerPtr, TrackPtr, Entries>;
using TimeInfos = std::vector<TimeInfo>;
using Arrangers = std::vector<ArrangerPtr>;

struct IProject {
  public:
    virtual ~IProject() noexcept = default;

    // Arranger
    virtual ArrangerPtr create() noexcept = 0;
    virtual std::optional<ArrangerPtr> getArranger(size_t arrangerId) noexcept = 0;
    virtual Arrangers getArrangers() const noexcept = 0;
    virtual void removeArranger(size_t arrangerId) noexcept = 0;
    virtual void clearArranger(size_t arrangerId) noexcept = 0;
    virtual void clearArrangers() noexcept = 0;

    // Track
    virtual std::optional<Tracks> getTracks(size_t arrangerId) const noexcept = 0;
    virtual void removeTrack(size_t arrangerId, size_t trackIndex) noexcept = 0;
    virtual void clearTrack(size_t arrangerId, size_t trackIndex) noexcept = 0;

    // Entry
    virtual void addEntry(size_t arrangerId, size_t trackIndex, Entry entry) noexcept = 0;
    virtual void removeEntry(size_t entryId) noexcept = 0;

    // Time-based
    virtual TimeInfos at(clk::Nanos position) const noexcept = 0;
    virtual TimeInfos within(clk::Nanos start, clk::Nanos end) const noexcept = 0;
    virtual std::optional<TimeInfos> at(size_t arrangerId, clk::Nanos position) const noexcept = 0;
    virtual std::optional<TimeInfos> within(size_t arrangerId, clk::Nanos start, clk::Nanos end) const noexcept = 0;
};
} // namespace pro
