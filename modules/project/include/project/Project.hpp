#pragma once

#include "IProject.hpp"

namespace pro {

class Project : public IProject {
  public:
    Project();
    ~Project();

    // Arranger
    ArrangerPtr create() noexcept;
    std::optional<ArrangerPtr> getArranger(size_t arrangerId) noexcept;
    Arrangers getArrangers() const noexcept;
    void removeArranger(size_t arrangerId) noexcept;
    void clearArranger(size_t arrangerId) noexcept;
    void clearArrangers() noexcept;

    // Track
    std::optional<Tracks> getTracks(size_t arrangerId) const noexcept;
    void removeTrack(size_t arrangerId, size_t trackIndex) noexcept;
    void clearTrack(size_t arrangerId, size_t trackIndex) noexcept;

    // Entry
    void addEntry(size_t arrangerId, size_t trackIndex, Entry entry) noexcept;
    void removeEntry(size_t entryId) noexcept;

    // Time-based
    TimeInfos at(clk::Nanos position) const noexcept;
    TimeInfos within(clk::Nanos start, clk::Nanos end) const noexcept;
    std::optional<TimeInfos> at(size_t arrangerId, clk::Nanos position) const noexcept;
    std::optional<TimeInfos> within(size_t arrangerId, clk::Nanos start, clk::Nanos end) const noexcept;

  private:
    struct pImpl;
    std::unique_ptr<pImpl> p;
};
} // namespace pro
