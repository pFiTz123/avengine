#pragma once

#include "IArranger.hpp"

namespace pro {
class Arranger : public IArranger {
  public:
    Arranger() noexcept;
    size_t id() const noexcept override;
    clk::Nanos start() const noexcept override;
    clk::Nanos end() const noexcept override;
    clk::Nanos duration() const noexcept override;
    void addEntry(size_t trackIndex, Entry entry) noexcept override;
    void addTrack() noexcept override;
    void removeEntry(size_t entryId) noexcept override;
    void removeTrack(size_t trackIndex) noexcept override;
    Tracks tracks() const noexcept override;
    void clear() noexcept override;
    void swap(size_t trackA, size_t trackB) noexcept override;
    size_t size() const noexcept override;

  private:
    size_t id_;
    std::vector<TrackPtr> tracks_;
};
} // namespace pro
