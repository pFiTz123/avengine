#pragma once

#include "ITrack.hpp"

namespace pro {
class Track : public ITrack {
  public:
    Track() noexcept = default;
    Track(size_t index) noexcept;
    size_t index() const noexcept override;
    void setIndex(size_t index) noexcept override;
    void add(Entry entry) noexcept override;
    Entries at(clk::Nanos clk) const noexcept override;
    Entries within(clk::Nanos start, clk::Nanos end) const noexcept override;
    std::optional<Entry> get(size_t id) const noexcept override;
    Entries getEntries() const noexcept override;
    void remove(Entry entry) noexcept override;
    void update(Entry entry) noexcept override;
    size_t size() const noexcept override;
    clk::Nanos start() const noexcept override;
    clk::Nanos end() const noexcept override;
    clk::Nanos duration() const noexcept override;
    void remove(size_t entryId) noexcept override;
    void clear() noexcept override;

  protected:
    size_t index_ = 0;
    Entries entries;
};
} // namespace pro
