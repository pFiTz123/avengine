#include "project/Arranger.hpp"
#include "project/EntryFactory.hpp"
#include "gtest/gtest.h"
#include <functional>

namespace {
constexpr auto createEntry = static_cast<std::optional<pro::Entry> (*)(int64_t, int64_t)>(pro::EntryFactory::create);
}

TEST(ArrangerTest, Add)
{
    pro::Arranger arranger;
    arranger.addEntry(0, *createEntry(0, 10));
    arranger.addEntry(3, *createEntry(0, 10));
    EXPECT_EQ(4, arranger.size());
    arranger.addEntry(1, *createEntry(7, 20));
    arranger.addEntry(2, *createEntry(7, 20));
    EXPECT_EQ(4, arranger.size());
    arranger.addEntry(10, *createEntry(7, 20));
    EXPECT_EQ(11, arranger.size());
}

TEST(ArrangerTest, Remove)
{
    pro::Arranger arranger;
    arranger.addEntry(0, *createEntry(0, 10));
    arranger.addEntry(1, *createEntry(7, 20));
    arranger.addEntry(2, *createEntry(10, 30));
    EXPECT_EQ(3, arranger.size());
    arranger.removeTrack(0);
    EXPECT_EQ(2, arranger.size());
    arranger.addTrack();
    arranger.addTrack();
    EXPECT_EQ(4, arranger.size());
}

TEST(ArrangerTest, Duration)
{
    pro::Arranger arranger;
    arranger.addEntry(0, *createEntry(2, 10));
    arranger.addEntry(1, *createEntry(7, 20));
    arranger.addEntry(2, *createEntry(10, 30));
    EXPECT_EQ(clk::Nanos{28}, arranger.duration());
}

TEST(ArrangerTest, Start)
{
    pro::Arranger arranger;
    arranger.addEntry(0, *createEntry(2, 10));
    arranger.addEntry(1, *createEntry(7, 20));
    arranger.addEntry(2, *createEntry(4, 8));
    arranger.addEntry(7, *createEntry(8, 30));
    auto e = arranger.start();
    EXPECT_EQ(2, e.count());
}

TEST(ArrangerTest, End)
{
    pro::Arranger arranger;
    arranger.addEntry(0, *createEntry(2, 10));
    arranger.addEntry(1, *createEntry(7, 20));
    arranger.addEntry(2, *createEntry(4, 8));
    arranger.addEntry(7, *createEntry(8, 30));
    auto e = arranger.end();
    EXPECT_EQ(30, e.count());
}


TEST(ArrangerTest, TrackIndex)
{
	pro::Arranger arranger;
	arranger.addEntry(0, *createEntry(0, 10));
	arranger.addEntry(1, *createEntry(7, 20));
	arranger.addEntry(2, *createEntry(10, 30));
	EXPECT_EQ(3, arranger.size());
	arranger.removeTrack(1);
	EXPECT_EQ(2, arranger.size());
	auto tracks = arranger.tracks();
	for (size_t i = 0; i < tracks.size(); ++i) {
		EXPECT_EQ(i, tracks.at(i)->index());
	}
	arranger.addTrack();
	arranger.addTrack();
	EXPECT_EQ(4, arranger.size());
}