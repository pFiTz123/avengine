#include "project/Track.hpp"
#include "project/EntryFactory.hpp"
#include "gtest/gtest.h"

using namespace pro;

TEST(TrackTest, Add)
{
    Track track;
    track.add(*EntryFactory::create(0, 10));
    EXPECT_EQ(1, track.size());

    track.add(*EntryFactory::create(10, 20));
    track.add(*EntryFactory::create(2, 12));
    EXPECT_EQ(3, track.size());
}

TEST(TrackTest, Remove)
{
    Track track;
    Entry entry = *EntryFactory::create(10, 20);
    track.add(*EntryFactory::create(0, 10));
    track.add(entry);
    track.add(*EntryFactory::create(2, 12));

    EXPECT_EQ(3, track.size());

    track.remove(entry.id());
    EXPECT_EQ(2, track.size());
}

TEST(TrackTest, GetByTime)
{
    Track track;
    Entry entry1 = *EntryFactory::create(0, 14);
    Entry entry2 = *EntryFactory::create(10, 20);
    Entry entry3 = *EntryFactory::create(5, 15);
    track.add(entry1);
    track.add(entry2);
    track.add(entry3);

    auto entries = track.at(clk::Nanos{10});
    EXPECT_EQ(3, entries.size());

    entries = track.at(clk::Nanos{15});
    EXPECT_EQ(1, entries.size());

    entries = track.at(clk::Nanos{1});
    EXPECT_EQ(1, entries.size());
}

TEST(TrackTest, GetWithinRange)
{
    Track track;
    Entry entry1 = *EntryFactory::create(0, 14);
    Entry entry2 = *EntryFactory::create(10, 20);
    Entry entry3 = *EntryFactory::create(5, 15);
    track.add(entry1);
    track.add(entry2);
    track.add(entry3);

    auto within1 = track.within(clk::Nanos{0}, clk::Nanos{7});
    EXPECT_EQ(2, within1.size());

    auto within2 = track.within(clk::Nanos{14}, clk::Nanos{15});
    EXPECT_EQ(2, within2.size());

    auto within3 = track.within(clk::Nanos{20}, clk::Nanos{25});
    EXPECT_TRUE(within3.empty());
}

TEST(TrackTest, GetById)
{
    Track track;
    Entry entry1 = *EntryFactory::create(0, 14);
    Entry entry2 = *EntryFactory::create(10, 20);
    Entry entry3 = *EntryFactory::create(5, 15);
    track.add(entry1);
    track.add(entry2);
    track.add(entry3);

    auto entry = track.get(entry2.id());
    ASSERT_TRUE(entry.has_value());
    EXPECT_EQ(entry2, entry.value());

    auto nextId = entry3.id() + 1;
    EXPECT_FALSE(track.get(nextId).has_value());
}

TEST(TrackTest, Duration)
{
    Track track;
    track.add(*EntryFactory::create(7, 70));
    track.add(*EntryFactory::create(5, 50));
    track.add(*EntryFactory::create(9, 90));

    EXPECT_EQ(clk::Nanos{85}, track.duration());
}
