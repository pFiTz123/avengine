#include "project/EntryFactory.hpp"
#include "gtest/gtest.h"

using namespace pro;

TEST(EntryTest, Create)
{
    EXPECT_FALSE(EntryFactory::create(20, 10).has_value());
    EXPECT_TRUE(EntryFactory::create(10, 20).has_value());
}
